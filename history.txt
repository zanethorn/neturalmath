0.1.0	DO NOT USE!  Basic interpreter and parser, support for doubles only

0.2.0	Added support for scalar data types and keywords
0.2.1	Rewrote parsing engine to use a forward-processing syntax tree instead of nested loops.  Nearly doubles average line processing performance.
0.2.2	Extract all literals into resource files (except regular expressions)
0.2.3	Add basic project documentation