﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Expressions;

namespace NeturalMath.UnitTests.ExpressionTests
{
    [TestClass]
    public class VariableAssignmentTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }


        #region Access Level Permutations

        [TestMethod]
        public void TestBasicVariableAssignment()
        {
            var testString = "x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestPublicVariableAssignment()
        {
            var testString = "public x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Public, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestGlobalVariableAssignment()
        {
            var testString = "global x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Global, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestPublicConstVariableAssignment()
        {
            var testString = "public const x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.PublicConst, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestGlobalConstVariableAssignment()
        {
            var testString = "global const x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.GlobalConst, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestPublicReadOnlyVariableAssignment()
        {
            var testString = "public readonly x = 3";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.PublicReadOnly, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(3.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        

        #endregion

        #region Data Assignment Permutations

        [TestMethod]
        public void TestNumberAssignment()
        {
            var testString = "x = 3 + 2";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(5.0, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestStringAssignment()
        {
            var testString = "x = 'this is a string'";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual("this is a string", variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestTrueAssignment()
        {
            var testString = "x = true";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(true, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestFalseAssignment()
        {
            var testString = "x = false";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(false, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        [TestMethod]
        public void TestVoidAssignment()
        {
            var testString = "x = void";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(null, variable.Value.Execute().Value, "Tested variable did not have the expected value");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");
        }

        #endregion

        #region Implicit Assignment Tests

        [TestMethod]
        public void TestImplicitAssignment()
        {
            var testString = "x = y";
            var result = Runtime.ExecuteText(testString);

            Assert.AreEqual(Runtime.Void, result, "Result of assignment expression was not void");

            Assert.IsTrue(Runtime.CurrentDomain.HasVariable("x"),
                          "Current Domain does not contain the tested variable definition");

            var variable = Runtime.CurrentDomain.GetVariable("x");
            Assert.IsNotNull(variable, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("x", variable.Name, "Tested variable did not have the expected name");
            Assert.IsTrue(variable.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, variable.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(Runtime.CurrentDomain, variable.ParentScope, "Tested variable did not have the expected parent domain");

            var dependent = Runtime.CurrentDomain.GetVariable("y");
            Assert.IsNotNull(dependent, "Current Domain returned null for tested variable definition");
            Assert.AreEqual("y", dependent.Name, "Tested variable did not have the expected name");
            Assert.IsFalse(dependent.IsSet, "Tested variable was not set to a value");
            Assert.AreEqual(Scope.Private, dependent.Scope, "Tested variable did not have the expected access type");
            Assert.AreEqual(Runtime.CurrentDomain, dependent.ParentScope, "Tested variable did not have the expected parent domain");
        }

        #endregion
    }
}
