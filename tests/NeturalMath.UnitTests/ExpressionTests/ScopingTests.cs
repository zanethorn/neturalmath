﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.ExpressionTests
{
    [TestClass]
    public class ScopingTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }


        [TestMethod]
        public void TestVariablePointers1()
        {
            var inputText = 
@"
public test = {
    public value1 = 3
    public value2 = value1 + 3
} 

a = test
b = a

a.value1 = 10
b.value1 = 20
";
            Runtime.ExecuteText(inputText);

            var aValue1 = (Variable)Runtime.RootDomain.GetSymbol("a.value1");
            var aValue2 = (Variable)Runtime.RootDomain.GetSymbol("a.value2");
            var bValue1 = (Variable)Runtime.RootDomain.GetSymbol("b.value1");
            var bValue2 = (Variable)Runtime.RootDomain.GetSymbol("b.value2");

            Assert.AreEqual(Runtime.NewNumber(10), aValue1.Invoke());
            Assert.AreEqual(Runtime.NewNumber(13), aValue2.Invoke());
            Assert.AreEqual(Runtime.NewNumber(20), bValue1.Invoke());
            Assert.AreEqual(Runtime.NewNumber(23), bValue2.Invoke());
        }

        [TestMethod]
        public void TestPublicScope1()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    public value2 = value1 + 3
} 


d = test.value1
";
            Runtime.ExecuteText(inputText);

            var value1 = (Variable)Runtime.RootDomain.GetSymbol("test.value1");
            var value2 = (Variable)Runtime.RootDomain.GetSymbol("test.value2");
            var d = (Variable)Runtime.RootDomain.GetSymbol("d");

            Assert.AreEqual(Runtime.NewNumber(3), value1.Invoke());
            Assert.AreEqual(Runtime.NewNumber(6), value2.Invoke());

            Assert.AreEqual(Runtime.NewNumber(3), d.Invoke());
        }


        [TestMethod]
        public void TestPrivateConstScope1()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    private const value2 = value1 + 3

    value2 = 6
} 


d = test.value2
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.ConstantModified, ex.ErrorCode);
            }
        }

        [TestMethod]
        public void TestPrivateConstScope2()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    const value2 = value1 + 3

    value2 = 6
} 


d = test.value2
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.ConstantModified, ex.ErrorCode);
            }
        }

        [TestMethod]
        public void TestPublicConstScope1()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    public const value2 =  3
} 


test.value2 = 6
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.ConstantModified, ex.ErrorCode);
            }
        }

        [TestMethod]
        public void TestPublicReadonlyScope1()
        {
            var inputText =
@"
public test = {
    public readonly value1 = 3
    
    value1 = 4
} 

test.value1 = 5
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.CannotModifyReadonlyVariable, ex.ErrorCode);
            }
        }


        [TestMethod]
        public void TestPublicScope2()
        {
            var inputText =
@"
public test = {
    public readonly value1 = 3
    
    value1 = 4
} 

";
            Runtime.ExecuteText(inputText);

            var value1 = (Variable)Runtime.RootDomain.GetSymbol("test.value1");

            Assert.AreEqual(Runtime.NewNumber(4), value1.Invoke());
        }


        [TestMethod]
        public void TestPrivateScope1()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    private value2 = value1 + 3
} 


d = test.value2
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.CannotAccessPrivateOrLocalMember, ex.ErrorCode);
            }
        }

        [TestMethod]
        public void TestGlobalScope1()
        {
            var inputText =
@"
public test = {
    global value1 = 3
    public value2 = value1 + 3
} 

";
            Runtime.ExecuteText(inputText);

            var value1 = (Variable)Runtime.RootDomain.GetSymbol("test.value1");
            var value2 = (Variable)Runtime.RootDomain.GetSymbol("value1");

            Assert.AreEqual(value1, value2);
        }

        [TestMethod]
        public void TestGlobalConstScope1()
        {
            var inputText =
@"
public test = {
    public value1 = 3
    global const value2 =  3
} 


value2 = 6
";
            try
            {
                Runtime.ExecuteText(inputText);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (MathException ex)
            {
                Assert.AreEqual(ErrorCodes.ConstantModified, ex.ErrorCode);
            }
        }
    
    }
}
