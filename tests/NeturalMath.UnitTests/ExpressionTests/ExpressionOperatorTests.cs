﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.ExpressionTests
{
    [TestClass]
    public class ExpressionOperatorTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        [TestMethod]
        public void TestInterpretMinus1()
        {
            var input = "-3";
            var expected = Runtime.NewNumber(-3);

            var result = Runtime.ExecuteText(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestInterpretMinus2()
        {
            var input = "5 + -3";
            var expected = Runtime.NewNumber(2);

            var result = Runtime.ExecuteText(input);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestInterpretMinus3()
        {
            var input = "5 - -3";
            var expected = Runtime.NewNumber(8);

            var result = Runtime.ExecuteText(input);

            Assert.AreEqual(expected, result);
        }
    }
}
