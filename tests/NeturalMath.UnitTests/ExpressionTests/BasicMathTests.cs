﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.ExpressionTests
{
    [TestClass]
    public class BasicMathTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        [TestMethod]
        public void TestBasicAdd()
        {
            var testString = "2 + 3";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double) result.Value;

            Assert.AreEqual(5.0, resultValue,"Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestBasicSubtract()
        {
            var testString = "5 - 4";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(1.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestInvertedSubtract()
        {
            var testString = "2 - 3";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(-1.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestBasiMultiply()
        {
            var testString = "2 * 3";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(6.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestBasicDivide()
        {
            var testString = "8 / 4";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(2.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestDecimalDivide()
        {
            var testString = "13.2 / 8.1";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(1.62962963, resultValue, 1e-9);
        }

        [TestMethod]
        public void TestBasicMod()
        {
            var testString = "9 % 4";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(1.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestDecimalMod()
        {
            var testString = "13.2 % 8.1";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(5.1, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestBasicPower()
        {
            var testString = "2 ^ 3";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(8.0, resultValue, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestDecimalPower()
        {
            var testString = "13.2 ^ 2.2";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(291.9191919191919, resultValue,1e-9, "Evaluation results did not match expected results");
        }

        [TestMethod]
        public void TestNegativePower()
        {
            var testString = "2 ^ -3";
            var result = Runtime.ExecuteText(testString);

            var resultValue = (double)result.Value;

            Assert.AreEqual(0.125, resultValue, "Evaluation results did not match expected results");
        }

    }
}
