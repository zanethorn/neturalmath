﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.LoadTests
{
    [TestClass]
    public class TestRandomMath
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        [TestMethod]
        public void TestRandomFunctions1()
        {
            var rnd = new Random();
            var opCode = rnd.Next(3);
            var left = rnd.NextDouble()*10000;
            var right = rnd.NextDouble()*10000;
            string op = null;
            Func<double, double, double> f = null; ;
            switch (opCode)
            {
                case 0:
                    op = "+";
                    f = (l, r) => l + r;
                    break;
                case 1:
                    op = "-";
                    f = (l, r) => l - r;
                    break;
                case 2:
                    op = "*";
                    f = (l, r) => l * r;
                    break;
                case 3:
                    op = "/";
                    f = (l, r) => l / r;
                        break;
            }

            var command = left.ToString() + op + right.ToString();

            var expected = f(left, right);
            var result = Runtime.ExecuteText(command);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value,0.00000001);
        }
    }
}
