﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Expressions;

namespace NeturalMath.UnitTests.VisitorTests
{
    [TestClass]
    public class SimplifyingVisitorTest
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        private string CompileAndReturnStrings(string myString)
        {
            var expression = Runtime.InterpretText(myString);
            var visitor = new SimplifyingVisitor(Runtime);
            var def = new DefVisitor(Runtime);
            var simplified = visitor.Visit(expression);

            return def.Visit(simplified);
        }

        [TestMethod]
        public void TestMergeConstantsInFunction()
        {

        }
    }
}
