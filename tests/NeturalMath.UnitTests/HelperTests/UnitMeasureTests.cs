﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.HelperTests
{
    [TestClass]
    public class UnitMeasureTests
    {
        #region ToString tests

        [TestMethod]
        public void TestUnitMeasureToString1()
        {
            var target = new UnitMeasure(UnitDimension.Meter);
            var expected = "m";

            var result = target.ToString();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestUnitMeasureToString2()
        {
            var target = new UnitMeasure(UnitDimension.Meter*UnitDimension.Meter);
            var expected = "m^2";

            var result = target.ToString();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestUnitMeasureToString3()
        {
            var target = new UnitMeasure(UnitDimension.Meter,UnitDimension.Seconds*UnitDimension.Seconds);
            var expected = "m*s^2";

            var result = target.ToString();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestUnitMeasureToString4()
        {
            var target = new UnitMeasure(UnitDimension.Meter, UnitDimension.Seconds / UnitDimension.Seconds/ UnitDimension.Seconds);
            var expected = "m/s^2";

            var result = target.ToString();
            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Operator Tests

        [TestMethod]
        public void TestMultiplyMeasure1()
        {
            var value1 = new UnitMeasure(UnitDimension.Meter);
            var value2 = new UnitMeasure(UnitDimension.Meter);

            var result = value1 * value2;
            var d = result[MeasurementTypes.Length];
            Assert.AreEqual(2, d.Order);
            Assert.AreEqual("m", d.Symbol);
            Assert.AreEqual("Meters", d.Name);
            Assert.AreEqual(MeasurementTypes.Length, d.Type);
            Assert.AreEqual(1, d.ConversionFromBase(1));
        }

        [TestMethod]
        public void TestDivideMeasure1()
        {
            var value1 = new UnitMeasure(UnitDimension.Meter);
            var value2 = new UnitMeasure(UnitDimension.Meter);

            var result = value1 / value2;
            var d = result[MeasurementTypes.Length];
            Assert.IsNull(d);
        }

        #endregion

    }
}
