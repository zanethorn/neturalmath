﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.HelperTests
{
    [TestClass]
    public class DoubleExtensions
    {
        [TestMethod]
        public void DoubleXorTest1()
        {
            var value1 = 5.0;
            var value2 = 7.0;

            var expected = 2.0;
            var result = value1.Xor(value2);

            Assert.AreEqual(expected, result);
        }
    }
}
