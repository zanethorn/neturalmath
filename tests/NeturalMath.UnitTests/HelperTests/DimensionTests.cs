﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.HelperTests
{
    [TestClass]
    public class DimensionTests
    {
        [TestMethod]
        public void TestMetersSquared1()
        {
            var value1 = UnitDimension.Meter;
            var value2 = UnitDimension.Meter;

            var result = value1 * value2;
            
            Assert.AreEqual(2, result.Order);
            Assert.AreEqual("m", result.Symbol);
            Assert.AreEqual("Meters", result.Name);
            Assert.AreEqual(MeasurementTypes.Length, result.Type);
        }

        [TestMethod]
        public void TestConversionCmSquaredToMSquared1()
        {
            var value1 = UnitDimension.Centimeters;
            var value2 = UnitDimension.Centimeters;

            var result = value1 * value2;

            var conversion = result.ConversionFromBase;

            var metersSquared = 1;
            var cmSquared = conversion(metersSquared);

            Assert.AreEqual(10000, cmSquared);
        }
    }
}
