﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.DataTypeTests
{
    [TestClass]
    public class ComplexOperatorTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        public MathRuntime Runtime { get; private set; }

        

        #region Binary Functions

        #region Add Tests

        [TestMethod]
        public void TestAddComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected =Runtime.NewComplex(3,5);

            var result = value1 + value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAddComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(1,1);

            var result = value1 + value2;
            
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestAddComplexInterpreted1()
        {
            var testValue = "(1,2i) + (2,3i)";
            var expected = Runtime.NewComplex(3,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAddComplexInterpreted2()
        {
            var testValue = "(0,1i) + (1,0i)";
            var expected = Runtime.NewComplex(1,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Subtract Tests

        [TestMethod]
        public void TestSubtractComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(-1,-1);

            var result = value1 - value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSubtractComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(-1,1);

            var result = value1 - value2;
            
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestSubtractComplexInterpreted1()
        {
            var testValue = "(1,2i) - (2,3i)";
            var expected = Runtime.NewComplex(-1,-1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSubtractComplexInterpreted2()
        {
            var testValue = "(0,1i) - (1,0i)";
            var expected = Runtime.NewComplex(-1,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Multiply Tests

        [TestMethod]
        public void TestMultiplyComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(8,7);

            var result = value1 * value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplyComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(0,1);

            var result = value1 * value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplyComplexInterpreted1()
        {
            var testValue = "(1,2i) * (2,3i)";
            var expected = Runtime.NewComplex(8,7);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplyComplexInterpreted2()
        {
            var testValue = "(0,1i) * (1,0i)";
            var expected = Runtime.NewComplex(0,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Divide Tests

        [TestMethod]
        public void TestDivideComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(0.6153846153846154, 0.07692307692307693);

            var result = value1 / value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(0,1);

            var result = value1 / value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideComplexInterpreted1()
        {
            var testValue = "(1,2i) / (2,3i)";
            var expected = Runtime.NewComplex(0.6153846153846154, 0.07692307692307693);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideComplexInterpreted2()
        {
            var testValue = "(0,1i) / (1,0i)";
            var expected = Runtime.NewComplex(0,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Modulus Tests

        [TestMethod]
        public void TestModulusComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(3,5);

            var result = value1 % value2;
            
            Assert.AreEqual(expected, result);
            
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestModulusComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,1);
            var expected = Runtime.NewComplex(1,0);

            var result = value1 % value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusComplexInterpreted1()
        {
            var testValue = "(1,2i) % (2,3i)";
            var expected = Runtime.NewComplex(3,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);

            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestModulusComplexInterpreted2()
        {
            var testValue = "(0,1i) % (1,1i)";
            var expected = Runtime.NewComplex(1,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);

            Assert.Inconclusive("This unit test is not finished!");
        }

        #endregion

        #region And Tests

        [TestMethod]
        public void TestAndComplexInternal1()
        {
            var value1 = Runtime.NewComplex(3,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(2,2);

            var result = value1 & value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAndComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(0,0);

            var result = value1 & value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAndComplexInterpreted1()
        {
            var testValue = "(3,2i) & (2,3i)";
            var expected = Runtime.NewComplex(2,2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAndComplexInterpreted2()
        {
            var testValue = "(0,1i) & (1,0i)";
            var expected = Runtime.NewComplex(0,0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Or Tests

        [TestMethod]
        public void TestOrComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(3,3);

            var result = value1 | value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestOrComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(1,1);

            var result = value1 | value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestOrComplexInterpreted1()
        {
            var testValue = "(1,2i) | (2,3i)";
            var expected = Runtime.NewComplex(3,3);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestOrComplexInterpreted2()
        {
            var testValue = "(0,1i) | (1,0i)";
            var expected = Runtime.NewComplex(1,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Xor Tests

        [TestMethod]
        public void TestXorComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(3,3);
            var expected = Runtime.NewComplex(2,1);

            var result = value1 ^ value2;
            
            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestXorComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(1,1);

            var result = value1 ^ value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestXorComplexInterpreted1()
        {
            var testValue = "(1,2i) >< (2,3i)";
            var expected = Runtime.NewComplex(3,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestXorComplexInterpreted2()
        {
            var testValue = "(0,1i) >< (1,0i)";
            var expected = Runtime.NewComplex(1,1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Equals Tests

        [TestMethod]
        public void TestEqualsComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse( value);
        }

        [TestMethod]
        public void TestEqualsComplexInternal2()
        {
            var value1 = Runtime.NewComplex(1,1);
            var value2 = Runtime.NewComplex(1,1);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value );
        }

        [TestMethod]
        public void TestEqualsComplexInterpreted1()
        {
            var testValue = "(1,2i) == (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestEqualsComplexInterpreted2()
        {
            var testValue = "(1,1i) == (1,1i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region NotEquals Tests

        [TestMethod]
        public void TestNotEqualsComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsComplexInternal2()
        {
            var value1 = Runtime.NewComplex(1,1);
            var value2 = Runtime.NewComplex(1,1);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsFalse( value );
        }

        [TestMethod]
        public void TestNotEqualsComplexInterpreted1()
        {
            var testValue = "(1,2i) <> (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsComplexInterpreted2()
        {
            var testValue = "(1,1i) <> (1,1i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse( value);
        }

        #endregion

        #region GreaterThan Tests

        [TestMethod]
        public void TestGreaterThanComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInternal2()
        {
            var value1 = Runtime.NewComplex(3, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInternal3()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInternal4()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInternal5()
        {
            var value1 = Runtime.NewComplex(2, 3);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInterpreted1()
        {
            var testValue = "(1,2i) > (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse( value);
        }

        [TestMethod]
        public void TestGreaterThanComplexInterpreted2()
        {
            var testValue = "(5,1i) > (1,0i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue( value);
        }

        #endregion

        #region GreaterThanOrEquals Tests

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInternal2()
        {
            var value1 = Runtime.NewComplex(3, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInternal3()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInternal4()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInternal5()
        {
            var value1 = Runtime.NewComplex(2, 3);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInterpreted1()
        {
            var testValue = "(1,2i) >= (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsComplexInterpreted2()
        {
            var testValue = "(5,1i) >= (1,0i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region LessThan Tests

        [TestMethod]
        public void TestLessThanComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanComplexInternal2()
        {
            var value1 = Runtime.NewComplex(3, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanComplexInternal3()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanComplexInternal4()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanComplexInternal5()
        {
            var value1 = Runtime.NewComplex(2, 3);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanComplexInterpreted1()
        {
            var testValue = "(1,2i) < (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanComplexInterpreted2()
        {
            var testValue = "(5,1i) < (1,0i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        #endregion

        #region LessThanOrEquals Tests


        [TestMethod]
        public void TestLessThanOrEqualsComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInternal2()
        {
            var value1 = Runtime.NewComplex(3, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInternal3()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 3);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInternal4()
        {
            var value1 = Runtime.NewComplex(2, 2);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInternal5()
        {
            var value1 = Runtime.NewComplex(2, 3);
            var value2 = Runtime.NewComplex(2, 2);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInterpreted1()
        {
            var testValue = "(1,2i) <= (2,3i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsComplexInterpreted2()
        {
            var testValue = "(5,1i) <= (1,0i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        #endregion

        #region Power Tests

        [TestMethod]
        public void TestPowerComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var value2 = Runtime.NewComplex(2,3);
            var expected = Runtime.NewComplex(3,5);

            var result = MathValue.PowerOperator(value1, value2);
            
            Assert.AreEqual(expected, result);
            
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestPowerComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,1);
            var value2 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(3,5);

            var result = MathValue.PowerOperator(value1, value2);
            
            Assert.AreEqual(expected, result);
            
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestPowerComplexInterpreted1()
        {
            var testValue = "(1,2i) ^ (2,3i)";
            var expected = Runtime.NewComplex(3,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);

            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestPowerComplexInterpreted2()
        {
            var testValue = "(0,1i) ^ (1,0i)";
            var expected = Runtime.NewComplex(3,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);

            Assert.Inconclusive("This unit test is not finished!");
        }

        #endregion

        #endregion

        #region Unary Functions

        #region Negative Tests

        [TestMethod]
        public void TestNegativeComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.NewComplex(-1,-2);
            
            var result = -value1 ;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNegativeComplexInternal2()
        {
            var value1 = Runtime.NewComplex(-3,1);
            var expected = Runtime.NewComplex(3,-1);

            var result = -value1;
            Assert.AreEqual(expected, result );

        }

        [TestMethod]
        public void TestNegativeComplexInterpreted1()
        {
            var testValue = "-(1,2i)";
            var expected = Runtime.NewComplex(-1,-2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNegativeComplexInterpreted2()
        {
            var testValue = "-(1,0i)";
            var expected = Runtime.NewComplex(-1,0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Positive Tests

        [TestMethod]
        public void TestPositiveComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.NewComplex(1,2);
            
            var result = +value1 ;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestPositiveComplexInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(1,0);

            var result = +value1;
            
            Assert.AreEqual(expected, result);
        }

        
        #endregion

        #region Not Tests

        [TestMethod]
        public void TestNotComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.False;
            
            var result = !value1 ;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNotComplexInternal2()
        {
            var value1 = Runtime.NewComplex(0,0);
            var expected = Runtime.True;

            var result = !value1;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNotComplexInterpreted1()
        {
            var testValue = "~(1,2i)";
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNotComplexInterpreted2()
        {
            var testValue = "~(0,0i)";
            var expected = Runtime.True;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region PlusPlus Tests

        [TestMethod]
        public void TestPlusPlusComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.NewComplex(2,2);
            
            var result = ++value1 ;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestPlusPlusComplexInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(2,0);

            var result = ++value1;
            
            Assert.AreEqual(expected, result);
        }

        #endregion

        #region MinusMinus Tests

        [TestMethod]
        public void TestMinusMinusComplexInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.NewComplex(0,2);
            
            var result = --value1 ;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMinusMinusComplexInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewComplex(0,0);

            var result = --value1;
            
            Assert.AreEqual(expected, result);
        }


        #endregion

        #endregion

        #region Conversion Functions

        #region Bool Functions

        [TestMethod]
        public void TestConvertComplexToBoolInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.True;

            var result = value1.ConvertToBoolean();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToBoolInternal2()
        {
            var value1 = Runtime.NewComplex(0,0);
            var expected = Runtime.False;

            var result = value1.ConvertToBoolean();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToBoolInterpreted1()
        {
            var testValue = "tobool((1,2i))";
            var expected = Runtime.True;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToBoolInterpreted2()
        {
            var testValue = "tobool((1,0i))";
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Number Functions

        [TestMethod]
        public void TestConvertComplexToNumberInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = Runtime.NewNumber(3);

            var result = value1.ConvertToNumber();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToNumberInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewNumber(1);

            var result = value1.ConvertToNumber();
            var value = result.Value;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToNumberInterpreted1()
        {
            var testValue = "tonumber((1,2i))";
            var expected = Runtime.NewNumber(1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToNumberInterpreted2()
        {
            var testValue = "tonumber((1,0i))";
            var expected = Runtime.NewNumber(1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Range Functions

        [TestMethod]
        public void TestConvertComplexToRangeInternal1()
        {
            var value1 = Runtime.NewComplex(3,4);
            var expected = Runtime.NewRange(3,5);

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToRangeInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewRange(1);

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToRangeInterpreted1()
        {
            var testValue = "torange((3,4i))";
            var expected = Runtime.NewRange(3,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToRangeInterpreted2()
        {
            var testValue = "torange((1,0i))";
            var expected = Runtime.NewRange(1);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Set Functions

        //[TestMethod]
        //public void TestConvertComplexToSetInternal1()
        //{
        //    var value1 = Runtime.NewComplex(1,2);
        //    var expected = Runtime.NewSet(value1);

        //    var result = value1.ConvertToSet();
        //    Assert.AreEqual(expected, result);
        //}

        //[TestMethod]
        //public void TestConvertComplexToSetInternal2()
        //{
        //    var value1 = Runtime.NewComplex(1,0);
        //    var expected = Runtime.NewSet(value1);

        //    var result = value1.ConvertToSet();
        //    Assert.AreEqual(expected, result);

        //}

        //[TestMethod]
        //public void TestConvertComplexToSetInterpreted1()
        //{
        //    var testValue = "toset((1,2i))";
        //    var expected = Runtime.NewSet(Runtime.NewComplex(1,2));

        //    // Call the method
        //    var result = Runtime.ExecuteText(testValue);

        //    Assert.AreEqual(expected, result);
        //}

        //[TestMethod]
        //public void TestConvertComplexToSetInterpreted2()
        //{
        //    var testValue = "toset((1,0i))";
        //    var expected = Runtime.NewComplex(1,0);

        //    // Call the method
        //    var result = Runtime.ExecuteText(testValue);

        //    Assert.AreEqual(expected, result);
        //}

        #endregion

        #region String Functions

        [TestMethod]
        public void TestConvertComplexToStringInternal1()
        {
            var value1 = Runtime.NewComplex(1,2);
            var expected = "(1,2i)";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertComplexToStringInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = "(1,0i)";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertComplexToStringInterpreted1()
        {
            var testValue = "tostring((1,2i))";
            var expected = "(1,2i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertComplexToStringInterpreted2()
        {
            var testValue = "tostring((1,0i))";
            var expected ="(1,0i)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Unit Functions

        [TestMethod]
        public void TestConvertComplexToUnitInternal1()
        {
            var value1 = Runtime.NewComplex(15,2);
            var expected = Runtime.NewUnit(15, UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToUnitInternal2()
        {
            var value1 = Runtime.NewComplex(1,0);
            var expected = Runtime.NewUnit(1,UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToUnitInterpreted1()
        {
            var testValue = "tounit((1,2i))";
            var expected = Runtime.NewUnit(1, UnitMeasure.Meters);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertComplexToUnitInterpreted2()
        {
            var testValue = "tounit((15,0i))";
            var expected = Runtime.NewUnit(15,UnitMeasure.Meters);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #endregion
        
    }
}
