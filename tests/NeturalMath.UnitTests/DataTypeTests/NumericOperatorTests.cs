﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.DataTypeTests
{
	[TestClass]
	public class NumericOperatorTests
	{

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }
		
        #region Binary Functions
        
        #region Add Tests

        [TestMethod]
        public void TestAddNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 8.0;

            var result = value1 + value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);

        }
        
        [TestMethod]
        public void TestAddNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = 10.5;

            var result = value1 + value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestAddNumberInterpreted1()
        {
            var testValue = "5.0 + 3.0";
            var expected = 8.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            
        }
        
        [TestMethod]
        public void TestAddNumberInterpreted2()
        {
            var testValue = "12.0 + -1.5";
            var expected = 10.5;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Subtract Tests

        [TestMethod]
        public void TestSubtractNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 2.0;

            var result = value1 - value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestSubtractNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = 13.5;

            var result = value1 - value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestSubtractNumberInterpreted1()
        {
            var testValue = "5.0 - 3.0";
            var expected = 2.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestSubtractNumberInterpreted2()
        {
            var testValue = "12.0 - -1.5";
            var expected = 13.5;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Multiply Tests

        [TestMethod]
        public void TestMultiplyNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 15.0;

            var result = value1 * value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestMultiplyNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = -18.0;

            var result = value1 * value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestMultiplyNumberInterpreted1()
        {
            var testValue = "5.0 * 3.0";
            var expected = 15.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestMultiplyNumberInterpreted2()
        {
            var testValue = "12.0 * -1.5";
            var expected = -18.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Divide Tests

        [TestMethod]
        public void TestDivideNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 1.6666666667;

            var result = value1 / value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestDivideNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = -8.0;

            var result = value1 / value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestDivideNumberInterpreted1()
        {
            var testValue = "5.0 / 3.0";
            var expected = 1.6666666667;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestDivideNumberInterpreted2()
        {
            var testValue = "12.0 / -1.5";
            var expected = -8.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Modulus Tests

        [TestMethod]
        public void TestModulusNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 2.0;

            var result = value1 % value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestModulusNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = 0.0;

            var result = value1 % value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestModulusNumberInterpreted1()
        {
            var testValue = "5.0 % 3.0";
            var expected = 2.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestModulusNumberInterpreted2()
        {
            var testValue = "12.0 % -1.5";
            var expected = 0.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region And Tests

        [TestMethod]
        public void TestAndNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 1.0;

            var result = value1 & value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestAndNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = 12.0;

            var result = value1 & value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestAndNumberInterpreted1()
        {
            var testValue = "5.0 & 3.0";
            var expected = 1.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestAndNumberInterpreted2()
        {
            var testValue = "12.0 & -1.5";
            var expected = 12;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Or Tests

        [TestMethod]
        public void TestOrNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 7.0;

            var result = value1 | value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestOrNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            var result = value1 | value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestOrNumberInterpreted1()
        {
            var testValue = "5.0 | 3.0";
            var expected = 7.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestOrNumberInterpreted2()
        {
            var testValue = "12.0 | -1.5";
            var expected = -1.5;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Xor Tests

        [TestMethod]
        public void TestXorNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 6.0;

            var result = value1 ^ value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestXorNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = -13.5;

            var result = value1 ^ value2;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestXorNumberInterpreted1()
        {
            var testValue = "5.0 >< 3.0";
            var expected = 6.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestXorNumberInterpreted2()
        {
            var testValue = "12.0 >< -1.5";
            var expected = -13.5;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion
           
        #region Equals Tests

        [TestMethod]
        public void TestEqualsNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }
        
        [TestMethod]
        public void TestEqualsNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestEqualsNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }
        
        [TestMethod]
        public void TestEqualsNumberInterpreted1()
        {
            var testValue = "5.0 == 3.0";
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse( value);
        }

        [TestMethod]
        public void TestEqualsNumberInterpreted2()
        {
            var testValue = "5.0 == 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestEqualsNumberInterpreted3()
        {
            var testValue = "3.0 == 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }
        
        #endregion
           
        #region NotEquals Tests

        [TestMethod]
        public void TestNotEqualsNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestNotEqualsNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsNumberInterpreted1()
        {
            var testValue = "5.0 <> 3.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsNumberInterpreted2()
        {
            var testValue = "5.0 <> 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestNotEqualsNumberInterpreted3()
        {
            var testValue = "3.0 <> 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }
        
        #endregion
           
        #region GreaterThan Tests
        [TestMethod]
        public void TestGreaterThansNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanNumberInterpreted1()
        {
            var testValue = "5.0 > 3.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanNumberInterpreted2()
        {
            var testValue = "5.0 > 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanNumberInterpreted3()
        {
            var testValue = "3.0 > 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }
        
        #endregion
           
        #region GreaterThanOrEquals Tests

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInterpreted1()
        {
            var testValue = "5.0 >= 3.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInterpreted2()
        {
            var testValue = "5.0 >= 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsNumberInterpreted3()
        {
            var testValue = "3.0 >= 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }
        
        #endregion
           
        #region LessThan Tests

        [TestMethod]
        public void TestLessThanNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanNumberInterpreted1()
        {
            var testValue = "5.0 < 3.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanNumberInterpreted2()
        {
            var testValue = "5.0 < 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanNumberInterpreted3()
        {
            var testValue = "3.0 < 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }
        
        #endregion
           
        #region LessThanOrEquals Tests

        [TestMethod]
        public void TestLessThanOrEqualsNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsNumberInternal2()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsNumberInternal3()
        {
            var value1 = Runtime.NewNumber(3.0);
            var value2 = Runtime.NewNumber(5.0);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsNumberInterpreted1()
        {
            var testValue = "5.0 <= 3.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsNumberInterpreted2()
        {
            var testValue = "5.0 <= 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsNumberInterpreted3()
        {
            var testValue = "3.0 <= 5.0";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }
        
        #endregion
           
        #region Power Tests

        [TestMethod]
        public void TestPowerNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var value2 = Runtime.NewNumber(3.0);
            var expected = 125.0;

            var result = MathValue.PowerOperator(value1, value2);
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestPowerNumberInternal2()
        {
            var value1 = Runtime.NewNumber(12.0);
            var value2 = Runtime.NewNumber(-1.5);
            var expected = 0.0240562612;

            var result = MathValue.PowerOperator(value1, value2);
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestPowerNumberInterpreted1()
        {
            var testValue = "5.0 ^ 3.0";
            var expected = 125.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestPowerNumberInterpreted2()
        {
            var testValue = "12.0 ^ -1.5";
            var expected = 0.0240562612;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        #endregion

        #endregion
        
        #region Unary Functions
        
        #region Negative Tests

        [TestMethod]
        public void TestNegativeNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = -5.0;
            
            var result = -value1 ;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestNegativeNumberInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            var result = -value1;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        [TestMethod]
        public void TestNegativeNumberInterpreted1()
        {
            var testValue = "-5.0";
            var expected = -5.0;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        [TestMethod]
        public void TestNegativeNumberInterpreted2()
        {
            var testValue = "-(-1.5)";
            var expected = 1.5;
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }
        
        #endregion

        #region Positive Tests

        [TestMethod]
        public void TestPositiveNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            
            var result = +value1 ;
            Assert.AreEqual(value1, result);
        }
        
        [TestMethod]
        public void TestPositiveNumberInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);

            var result = +value1;
            Assert.AreEqual(value1,result);
        }

        #endregion

        #region Not Tests

        [TestMethod]
        public void TestNotNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            
            var result = !value1 ;
            Assert.AreEqual(Runtime.False, result);

        }
        
        [TestMethod]
        public void TestNotNumberInternal2()
        {
            var value1 = Runtime.NewNumber(0);

            var result = !value1;
            Assert.AreEqual(Runtime.True,result);
        }

        [TestMethod]
        public void TestNotNumberInternal3()
        {
            var value1 = Runtime.NewNumber(double.NaN);

            var result = !value1;
            Assert.AreEqual(Runtime.False, result);
        }
        
        #endregion

        #region PlusPlus Tests

        [TestMethod]
        public void TestPlusPlusNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = 6.0;
            
            var result = ++value1 ;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestPlusPlusNumberInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = -0.5;

            var result = ++value1;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        
        #endregion
        
        #region MinusMinus Tests

        [TestMethod]
        public void TestMinusMinusNumberInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = 4.0;
            
            var result = --value1 ;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001);
        }
        
        [TestMethod]
        public void TestMinusMinusNumberInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = -2.5;

            var result = --value1;
            var value = (double)result.Value;
            Assert.AreEqual(expected, value,0.0000000001 );
        }
        
        
        #endregion
        
        
        #endregion
        
        #region Conversion Functions
        
        #region Bool Functions
        
        [TestMethod]
        public void TestConvertNumberToBoolInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);

            var result = value1.ConvertToBoolean();
            var value = (bool)result.Value;
            Assert.IsTrue( value);
        }
        
        [TestMethod]
        public void TestConvertNumberToBoolInternal2()
        {
            var value1 = Runtime.NewNumber(0.0);

            var result = value1.ConvertToBoolean();
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestConvertNumberToBoolInternal3()
        {
            var value1 = Runtime.NewNumber(double.NaN);

            var result = value1.ConvertToBoolean();
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }
        
        [TestMethod]
        public void TestConvertNumberToBoolInterpreted1()
        {
            var testValue = "tobool(5.0)";
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(Runtime.True,result);
        }
        
        [TestMethod]
        public void TestConvertNumberToBoolInterpreted2()
        {
            var testValue = "tobool(0)";
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestConvertNumberToBoolInterpreted3()
        {
            var testValue = "tobool(undefined)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(Runtime.False,result);
        }
        
        #endregion
        
        #region Complex Functions
        
        [TestMethod]
        public void TestConvertNumberToComplexInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = Runtime.NewComplex(5, 0);

            var result = value1.ConvertToComplex();
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToComplexInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewComplex(-1.5,0);

            var result = value1.ConvertToComplex();
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToComplexInterpreted1()
        {
            var testValue = "tocomplex(5.0)";
            var expected = Runtime.NewComplex(5,0);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToComplexInterpreted2()
        {
            var testValue = "tocomplex(-1.5)";
            var expected = Runtime.NewComplex(-1.5,0);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        #endregion
        
        #region Range Functions
        
        [TestMethod]
        public void TestConvertNumberToRangeInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = Runtime.NewRange(5, 5);

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);

        }
        
        [TestMethod]
        public void TestConvertNumberToRangeInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewRange(-1.5,-1.5);

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertNumberToRangeInternal3()
        {
            var value1 = Runtime.NewNumber(double.NaN);
            var expected = Runtime.Undefined;

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToRangeInterpreted1()
        {
            var testValue = "torange(5.0)";
            var expected = Runtime.NewRange(5,5);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToRangeInterpreted2()
        {
            var testValue = "torange(-1.5)";
            var expected = Runtime.NewRange(-1.5,-1.5);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertNumberToRangeInterpreted3()
        {
            var testValue = "torange(undefined)";
            var expected = Runtime.Undefined;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        #endregion
        
        #region Set Functions

        [TestMethod]
        public void TestConvertStringToSetInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);

            var result = value1.ConvertToSet();
            var value = (MathValue[])result.Value;
            Assert.AreEqual(value1, value[0]);
        }


        [TestMethod]
        public void TestConvertStringToSetInterpreted1()
        {
            var testValue = "toset(5.0)";
            var expected = Runtime.NewNumber(5.0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (MathValue[])result.Value;

            Assert.AreEqual(expected, value[0]);
        }
        
        #endregion
        
        #region String Functions
        
        [TestMethod]
        public void TestConvertNumberToStringInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = "5";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }
        
        [TestMethod]
        public void TestConvertNumberToStringInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = "-1.5";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInternal3()
        {
            var value1 = Runtime.NewNumber(double.NaN);
            var expected = "undefined";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInternal4()
        {
            var value1 = Runtime.NewNumber(double.PositiveInfinity);
            var expected = "infinity";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInternal5()
        {
            var value1 = Runtime.NewNumber(double.NegativeInfinity);
            var expected = "-infinity";

            var result = value1.ConvertToString();
            var value = result.Value;
            Assert.AreEqual(expected, value);
        }
        
        [TestMethod]
        public void TestConvertNumberToStringInterpreted1()
        {
            var testValue = "tostring(5.0)";
            var expected = "5";
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }
        
        [TestMethod]
        public void TestConvertNumberToStringInterpreted2()
        {
            var testValue = "tostring(-1.5)";
            var expected = "-1.5";
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInterpreted3()
        {
            var testValue = "tostring(undefined)";
            var expected = "undefined";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInterpreted4()
        {
            var testValue = "tostring(infinity)";
            var expected = "infinity";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertNumberToStringInterpreted5()
        {
            var testValue = "tostring(-infinity)";
            var expected = "-infinity";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = result.Value;

            Assert.AreEqual(expected, value);
        }
        
        #endregion
        
        #region Unit Functions
        
        [TestMethod]
        public void TestConvertNumberToUnitInternal1()
        {
            var value1 = Runtime.NewNumber(5.0);
            var expected = Runtime.NewUnit(5, UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToUnitInternal2()
        {
            var value1 = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewUnit(-1.5, UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);
            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToUnitInterpreted1()
        {
            var testValue = "tounit(5.0,#m)";
            var expected = Runtime.NewUnit(5, UnitMeasure.Meters);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        [TestMethod]
        public void TestConvertNumberToUnitInterpreted2()
        {
            var testValue = "tounit(-1.5,#m)";
            var expected = Runtime.NewUnit(-1.5, UnitMeasure.Meters);
            
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }
        
        #endregion

        #endregion
        
	}
}
