﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.DataTypeTests
{
    [TestClass]
    public class StringOperatorsTest
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Binary Functions

        #region Add Tests

        [TestMethod]
        public void TestAddStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = "foobar";

            var result = value1 + value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAddStringInternal2()
        {
            var value1 = Runtime.NewString("a b c d e f g");
            var value2 = Runtime.NewString(" ");
            var expected = "a b c d e f g ";

            var result = value1 + value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAddStringInterpreted1()
        {
            var testValue = "'foo' + 'bar'";
            var expected = "foobar";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAddStringInterpreted2()
        {
            var testValue = "'a b c d e f g' + ' '";
            var expected = "a b c d e f g ";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Subtract Tests

        [TestMethod]
        public void TestSubtractStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = "foo";

            var result = value1 - value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestSubtractStringInternal2()
        {
            var value1 = Runtime.NewString("a b c d e f g");
            var value2 = Runtime.NewString(" " );
            var expected = "abcdefg";

            var result = value1 - value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestSubtractStringInterpreted1()
        {
            var testValue = "'foo' - 'bar'";
            var expected = "foo";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestSubtractStringInterpreted2()
        {
            var testValue = "'a b c d e f g' - ' '";
            var expected = "abcdefg";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Multiply Tests

        [TestMethod]
        public void TestMultiplyStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1*value2;
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestMultiplyStringInternal2()
        {
            var value1 = Runtime.NewString("ab");
            var value2 = Runtime.NewString("3");
            var expected = "ababab";

            var result = value1 * value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestMultiplyStringInterpreted1()
        {
            var testValue = "'foo' * 'bar'";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected Exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestMultiplyStringInterpreted2()
        {
            var testValue = "'ab' *  '3'";
            var expected = "ababab";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Modulus Tests

        [TestMethod]
        public void TestModulusStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = Runtime.NewSet(value1);

            var result = value1 % value2;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("a");
            var expected = Runtime.NewSet(
                Runtime.EmptyString,
                Runtime.NewString("b"),
                Runtime.NewString("b"),
                Runtime.EmptyString
                );

            var result = value1 % value2;

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusStringInterpreted1()
        {
            var testValue = "'foo' % 'bar'";
            var expected = Runtime.NewSet(Runtime.NewString("foo"));
            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusStringInterpreted2()
        {
            var testValue = "'ababa % 'a'";
            var expected = Runtime.NewSet(
                Runtime.EmptyString,
                Runtime.NewString("b"),
                Runtime.NewString("b"),
                Runtime.EmptyString
                );

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Division Tests

        [TestMethod]
        public void TestDivideStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = Runtime.Zero;

            var result = value1 / value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("b");
            var expected = Runtime.NewNumber(2);

            var result = value1 / value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestDivideStringInterpreted1()
        {
            var testValue = "'foo' / 'bar'";
            var expected = Runtime.Zero;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideStringInterpreted2()
        {
            var testValue = "'ababa' / 'b'";
            var expected = Runtime.NewNumber(2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region And Tests

        [TestMethod]
        public void TestAndStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = string.Empty;

            var result = value1 & value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAndStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("a");
            var expected = "a";

            var result = value1 & value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAndStringInterpreted1()
        {
            var testValue = "'foo' & 'bar'";
            var expected = string.Empty;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestAndStringInterpreted2()
        {
            var testValue = "'ababa' & 'a'";
            var expected = "a";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Or Tests

        [TestMethod]
        public void TestOrStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = "fobar";

            var result = value1 | value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestOrStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("a");
            var expected = "ab";

            var result = value1 | value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestOrStringInterpreted1()
        {
            var testValue = "'foo' | 'bar'";
            var expected = "fobar";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestOrStringInterpreted2()
        {
            var testValue = "'ababa' | 'a'";
            var expected = "ab";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Xor Tests

        [TestMethod]
        public void TestXorStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = "fobar";

            var result = value1 ^ value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestXorStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("a");
            var expected = "b";

            var result = value1 ^ value2;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestXorStringInterpreted1()
        {
            var testValue = "'foo' >< 'bar'";
            var expected = "fobar";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestXorStringInterpreted2()
        {
            var testValue = "'ababa' ><  'a'";
            var expected = "b";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Equals Tests

        [TestMethod]
        public void TestEqualsStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestEqualsStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsTrue( value);
        }

        [TestMethod]
        public void TestEqualsStringInterpreted1()
        {
            var testValue = "'foo' == 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestEqualsStringInterpreted2()
        {
            var testValue = "'ababa' ==  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region NotEquals Tests

        [TestMethod]
        public void TestNotEqualsStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestNotEqualsStringInterpreted1()
        {
            var testValue = "'foo' <> 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsStringInterpreted2()
        {
            var testValue = "'ababa' <>  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        #endregion

        #region GreaterThan Tests

        [TestMethod]
        public void TestGreaterThanStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanStringInternal3()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("babab");

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanStringInterpreted1()
        {
            var testValue = "'foo' > 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanStringInterpreted2()
        {
            var testValue = "'ababa' >  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanStringInterpreted3()
        {
            var testValue = "'ababa' >  'babab'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        #endregion

        #region GreaterThanOrEquals Tests

        [TestMethod]
        public void TestGreaterThanOrEqualStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualStringInternal3()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("babab");

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualStringInterpreted1()
        {
            var testValue = "'foo' >= 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualStringInterpreted2()
        {
            var testValue = "'ababa' >=  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualStringInterpreted3()
        {
            var testValue = "'ababa' >=  'babab'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        #endregion

        #region LessThan Tests

        [TestMethod]
        public void TestLessThanStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanStringInternal3()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("babab");

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanStringInterpreted1()
        {
            var testValue = "'foo' < 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanStringInterpreted2()
        {
            var testValue = "'ababa' <  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanStringInterpreted3()
        {
            var testValue = "'ababa' <  'babab'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        #endregion

        #region LessThanOrEquals Tests

        [TestMethod]
        public void TestLessThanOrEqualsStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsStringInternal2()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("ababa");

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsStringInternal3()
        {
            var value1 = Runtime.NewString("ababa");
            var value2 = Runtime.NewString("babab");

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsStringInterpreted1()
        {
            var testValue = "'foo' <= 'bar'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsStringInterpreted2()
        {
            var testValue = "'ababa' <=  'ababa'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsStringInterpreted3()
        {
            var testValue = "'ababa' <=  'babab'";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        #endregion

        #region Power Tests

        [TestMethod]
        public void TestPowerStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var value2 = Runtime.NewString("bar");
            var expected = ErrorCodes.OperatorNotSupported;

            try
            {
                var result = MathValue.PowerOperator(value1, value2);
                Assert.Fail("Expected exception was not raised");
            }
            catch(MathException me)
            {
                Assert.AreEqual(expected, me.ErrorCode);
            }
        }


        [TestMethod]
        public void TestPowerStringInterpreted1()
        {
            var testValue = "'foo' ^ 'bar'";
            var expected = ErrorCodes.OperatorNotSupported;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not raised");
            }
            catch(MathException me)
            {
                Assert.AreEqual(expected, me.ErrorCode);
            }
        }


        #endregion

        #endregion

        #region Unary Functions

        #region Negative Tests

        [TestMethod]
        public void TestNegativeStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = "oof";

            var result = -value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNegativeStringInternal2()
        {
            var value1 = Runtime.NewString(" ");
            var expected = " ";

            var result = -value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNegativeStringInterpreted1()
        {
            var testValue = "-'foo'";
            var expected = "oof";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNegativeStringInterpreted2()
        {
            var testValue = "-' '";
            var expected = " ";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Positive Tests

        [TestMethod]
        public void TestPositiveStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = "foo";

            var result = +value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestPositiveStringInternal2()
        {
            var value1 = Runtime.NewString(" ");
            var expected = " ";

            var result = +value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestPositiveStringInterpreted1()
        {
            var testValue = "+'foo'";
            var expected = "foo";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestPositiveStringInterpreted2()
        {
            var testValue = "+' '";
            var expected = " ";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Not Tests

        [TestMethod]
        public void TestNotStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = "FOO";

            var result = !value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNotStringInternal2()
        {
            var value1 = Runtime.NewString("aBBa");
            var expected = "AbbA";

            var result = !value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNotStringInterpreted1()
        {
            var testValue = "~'foo'";
            var expected = "FOO";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestNotStringInterpreted2()
        {
            var testValue = "~'aBBa'";
            var expected = "AbbA";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        #endregion

        #region PlusPlus Tests

        [TestMethod]
        public void TestPlusPlusStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = "foo ";

            var result = ++value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestPlusPlusStringInternal2()
        {
            var value1 = Runtime.NewString("abba");
            var expected = "abba ";

            var result = ++value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        #endregion

        #region MinusMinus Tests

        [TestMethod]
        public void TestMinusMinusStringInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = "fo";

            var result = --value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestMinusMinusStringInternal2()
        {
            var value1 = Runtime.NewString("abba");
            var expected = "abb";

            var result = --value1;
            var value = (string)result.Value;
            Assert.AreEqual(expected, value);
        }


        #endregion

        #endregion

        #region Conversion Functions

        #region Bool Functions

        [TestMethod]
        public void TestConvertStringToBoolInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1.ConvertToBoolean();
                Assert.Fail("Expected exception was not rhrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToBoolInternal2()
        {
            var value1 = Runtime.NewString("true");

            var result = value1.ConvertToBoolean();
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestConvertStringToBoolInternal3()
        {
            var value1 = Runtime.NewString("false");

            var result = value1.ConvertToBoolean();
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestConvertStringToBoolInterpreted1()
        {
            var testValue = "tobool('foo')";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not rhrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToBoolInterpreted2()
        {
            var testValue = "tobool('true')";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestConvertStringToBoolInterpreted3()
        {
            var testValue = "tobool('false')";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        #endregion

        #region Complex Functions

        [TestMethod]
        public void TestConvertStringToComplexInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1.ConvertToComplex();
                Assert.Fail("Expected exception was not rhrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToComplexInternal2()
        {
            var value1 = Runtime.NewString("(1,2i)");
            var expectedReal = 1.0;
            var expectedI = 2.0;

            var result = (ComplexValue)value1.ConvertToComplex();
            Assert.AreEqual(expectedReal, result.Real.Value);
            Assert.AreEqual(expectedI, result.I.Value);
        }

        [TestMethod]
        public void TestConvertStringToComplexInternal3()
        {
            var value1 = Runtime.NewString("2i");
            var expectedReal = 0.0;
            var expectedI = 2.0;

            var result = value1.ConvertToComplex();
            var value = (ComplexValue)result.Value;
            Assert.AreEqual(expectedReal, value.Real);
            Assert.AreEqual(expectedI, value.I);
        }

        [TestMethod]
        public void TestConvertStringToComplexInterpreted1()
        {
            var testValue = "tocomplex('foo')";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not rhrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToComplexInterpreted2()
        {
            var testValue = "tocomplex('(1,2i)')";
            var expectedReal = 1.0;
            var expectedI = 2.0;

            var result = (ComplexValue)Runtime.ExecuteText(testValue);
            Assert.AreEqual(expectedReal, result.Real.Value);
            Assert.AreEqual(expectedI, result.I.Value);
        }

        [TestMethod]
        public void TestConvertStringToComplexInterpreted3()
        {
            var testValue = "tocomplex('2i')";
            var expectedReal = 0.0;
            var expectedI = 2.0;

            var result = (ComplexValue)Runtime.ExecuteText(testValue);
            Assert.AreEqual(expectedReal, result.Real.Value);
            Assert.AreEqual(expectedI, result.I.Value);
        }

        #endregion

        #region Number Functions

        [TestMethod]
        public void TestConvertStringToNumberInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1.ConvertToNumber();
                Assert.Fail("Expected exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToNumberInternal2()
        {
            var value1 = Runtime.NewString("-1.5");
            var expected = -1.5;

            var result = value1.ConvertToNumber();
            var value = (double)result.Value;
            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestConvertStringToNumberInternal3()
        {
            var value1 = Runtime.NewString("infinity");
            var expected = Runtime.Infinity;

            var result = value1.ConvertToNumber();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertStringToNumberInternal4()
        {
            var value1 = Runtime.NewString("-infinity");
            var expected = Runtime.NegativeInfinity;

            var result = value1.ConvertToNumber();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertStringToNumberInternal5()
        {
            var value1 = Runtime.NewString("undefined");
            var expected = Runtime.Undefined;

            var result = value1.ConvertToNumber();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertStringToNumberInterpreted1()
        {
            var testValue = "tonumber('foo')";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not rhrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToNumberInterpreted2()
        {
            var testValue = "tocomplex('-1.5')";
            var expected = -1.5;

            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;
            Assert.AreEqual(expected, value);
        }

        #endregion

        #region Range Functions

        [TestMethod]
        public void TestConvertStringToRangeInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1.ConvertToRange();
                Assert.Fail("Expected exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToRangeInternal2()
        {
            var value1 = Runtime.NewString("(-1)->10.5");
            var expected = Runtime.NewRange(-1.0,10.5);

            var result = value1.ConvertToRange();
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertStringToRangeInterpreted1()
        {
            var testValue = "torange('foo')";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToRangeInterpreted2()
        {
            var testValue = "torange('(-1)->10.5')";
            var expected = Runtime.NewRange(-1.0, 10.5);

            var result = Runtime.ExecuteText(testValue);
            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Set Functions

        [TestMethod]
        public void TestConvertStringToSetInternal1()
        {
            var value1 = Runtime.NewString("foo");

            var result = (SetValue)value1.ConvertToSet();
            var value = result.First().Execute();
            Assert.AreEqual(value1, value);
        }


        [TestMethod]
        public void TestConvertStringToSetInterpreted1()
        {
            var testValue = "toset('foo')";
            var expected = Runtime.NewSet(Runtime.NewString("foo"));

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Unit Functions

        [TestMethod]
        public void TestConvertStringToUnitInternal1()
        {
            var value1 = Runtime.NewString("foo");
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = value1.ConvertToUnit(UnitMeasure.Meters);
                Assert.Fail("Expected exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToUnitInternal2()
        {
            var value1 = Runtime.NewString("3");
            var expectedValue = 3.0;
            var expectedMeasure = UnitMeasure.Meters;

            var result = (UnitValue)value1.ConvertToUnit(UnitMeasure.Meters);
            var value = result.Value;
            var measure = result.Measure;
            Assert.AreEqual(expectedValue, value);
            Assert.AreEqual(expectedMeasure, measure);
        }

        [TestMethod]
        public void TestConvertStringToUnitInterpreted1()
        {
            var testValue = "tounit('foo','m')";
            var expected = ErrorCodes.StringCouldNotConvert;

            try
            {
                var result = Runtime.ExecuteText(testValue);
                Assert.Fail("Expected exception was not thrown");
            }
            catch (CastException ce)
            {
                Assert.AreEqual(expected, ce.ErrorCode);
            }
        }

        [TestMethod]
        public void TestConvertStringToUnitInterpreted2()
        {
            var testValue = "tounit('3','m')";
            var expectedValue = 3.0;
            var expectedMeasure = UnitMeasure.Meters;

            var result = (UnitValue) Runtime.ExecuteText(testValue);
            var value = result.Value;
            var measure = result.Measure;
            Assert.AreEqual(expectedValue, value);
            Assert.AreEqual(expectedMeasure, measure);
        }

        #endregion

        #endregion
        
    }
}
