﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.DataTypeTests
{
    [TestClass]
    public class BooleanOperatorTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        [TestMethod]
        public void TestAddBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 + value2;
            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestAddBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 + value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestAddBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 + value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestAddBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 + value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestSubtractBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 - value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestSubtractBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 - value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestSubtractBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 - value2;
            Assert.AreEqual(Runtime.True, result);
        }

        [TestMethod]
        public void TestSubtractBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 - value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestMultiplyBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 * value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestMultiplyBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 * value2;
            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestMultiplyBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 * value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestMultiplyBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 * value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestDivideBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 / value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestDivideBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 / value2;
            Assert.AreEqual(Runtime.Undefined, result);
        }

        [TestMethod]
        public void TestDivideBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 / value2;
            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestDivideBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 / value2;
            Assert.AreEqual(Runtime.Undefined, result);
        }

        [TestMethod]
        public void TestModBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 % value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestModBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 % value2;
            Assert.AreEqual(Runtime.Undefined, result);
        }

        [TestMethod]
        public void TestModBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 % value2;
            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestModBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 % value2;
            Assert.AreEqual(Runtime.Undefined, result);
        }

        [TestMethod]
        public void TestPowerBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = MathValue.PowerOperator(value1 , value2);
            var value = (bool)result.Value;
            Assert.AreEqual(Runtime.False, value);
        }

        [TestMethod]
        public void TestPowerBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = MathValue.PowerOperator( value1 , value2);
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestPowerBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = MathValue.PowerOperator(value1 , value2);
            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestPowerBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = MathValue.PowerOperator(value1 , value2);
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestAndBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 & value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestAndBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 & value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestAndBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 & value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestAndBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 & value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestOrBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 | value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestOrBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 | value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestOrBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 | value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestOrBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 | value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestXorBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 ^ value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestXorBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 ^ value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestXorBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 ^ value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestXorBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 ^ value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestEqualsBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestEqualsBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestEqualsBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestEqualsBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestNotEqualBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestNotEqualBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestNotEqualBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestNotEqualBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestGreaterThanBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestGreaterThanBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestGreaterThanBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestGreaterThanBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 >= value2;
            Assert.AreEqual(Runtime.True, result);
        }

        [TestMethod]
        public void TestLessThanOrEqualsBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestLessThanBoolsTrueTrue()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.True;

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestLessThanBoolsTrueFalse()
        {
            var value1 = (MathValue)Runtime.True;
            var value2 = (MathValue)Runtime.False;

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }

        [TestMethod]
        public void TestLessThanBoolsFalseTrue()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.True;

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.AreEqual(true, value);
        }

        [TestMethod]
        public void TestLessThanBoolsFalseFalse()
        {
            var value1 = (MathValue)Runtime.False;
            var value2 = (MathValue)Runtime.False;

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.AreEqual(false, value);
        }
    }
}
