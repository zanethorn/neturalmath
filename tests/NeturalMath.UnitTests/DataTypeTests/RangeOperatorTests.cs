﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.DataTypeTests
{
    [TestClass]
    public class RangeOperatorTests
    {

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Binary Functions

        #region Add Tests

        [TestMethod]
        public void TestAddRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(2, 10);
            var expected = Runtime.NewRange(3,12);

            var result = value1 + value2;
            Assert.AreEqual(expected, expected);
        }

        [TestMethod]
        public void TestAddRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(2,10);
            var expected = Runtime.NewRange(2, 11);

            var result = value1 + value2;
            Assert.AreEqual(expected, expected );
        }

        [TestMethod]
        public void TestAddRangeInterpreted1()
        {
            var testValue = "1->2 + 2->10";
            var expected = Runtime.NewRange(3, 12);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, expected);
        }

        [TestMethod]
        public void TestAddRangeInterpreted2()
        {
            var testValue = "0->1 + 0->?10";
            var expected = Runtime.NewRange(2, 11);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, expected);
        }

        #endregion

        #region Subtract Tests

        [TestMethod]
        public void TestSubtractRangeInternal1()
        {
            var value1 = Runtime.NewRange(2, 10);
            var value2 = Runtime.NewRange(1, 2);
            var expected =Runtime.NewRange(1,8);

            var result = value1 - value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSubtractRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var value2 = Runtime.NewRange(0,1);
            var expected = Runtime.NewRange(0, 9);

            var result = value1 - value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestSubtractRangeInterpreted1()
        {
            var testValue = "2->12 - 2->10";
            var expected = Runtime.NewRange(0,2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSubtractRangeInterpreted2()
        {
            var testValue = "0->10 - 0->1";
            var expected = Runtime.NewRange(0,9);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Multiply Tests

        [TestMethod]
        public void TestMultiplyRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(2, 10);
            var expected = Runtime.NewRange(2,20);

            var result = value1 * value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplyRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(3,10);
            var expected = Runtime.NewRange(0,10);

            var result = value1 * value2;
            Assert.AreEqual(expected, result );

        }

        [TestMethod]
        public void TestMultiplyRangeInterpreted1()
        {
            var testValue = "1->2 * 2->10";
            var expected = Runtime.NewRange(2, 20);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplyRangeInterpreted2()
        {
            var testValue = "0->1 * 3->10";
            var expected = Runtime.NewRange(0, 10);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Divide Tests

        [TestMethod]
        public void TestDivideRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 10);
            var value2 = Runtime.NewRange(2, 2);
            var expected = Runtime.NewRange(.5,5);

            var result = value1 / value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(0,10);
            var expected = Runtime.Undefined;

            var result = value1 / value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestDivideRangeInterpreted1()
        {
            var testValue = "1->10 / 2->2";
            var expected = Runtime.NewRange(.5,5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivideRangeInterpreted2()
        {
            var testValue = "1->1 / 1->10";
            var expected = Runtime.Undefined;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Modulus Tests

        [TestMethod]
        public void TestModulusRangeInternal1()
        {
            var value1 = Runtime.NewRange(5, 10);
            var value2 = Runtime.NewRange(2, 7);
            var expected =Runtime.NewRange(1,3);

            var result = value1 % value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusRangeInternal2()
        {
            var value1 = Runtime.NewRange(1,11);
            var value2 = Runtime.NewRange(1,10);
            var expected = Runtime.NewRange(0,1);

            var result = value1 % value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestModulusRangeInterpreted1()
        {
            var testValue = "5->7 % 2->10";
            var expected = Runtime.NewRange(1, 7);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestModulusRangeInterpreted2()
        {
            var testValue = "0->11 % 0->10";
            var expected = Runtime.Undefined;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region And Tests

        [TestMethod]
        public void TestAndRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 5);
            var value2 = Runtime.NewRange(2, 10);
            var expected = Runtime.NewRange(2,5);

            var result = value1 & value2;
            
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAndRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(3,10);
            var expected = Runtime.Void;

            var result = value1 & value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestAndRangeInterpreted1()
        {
            var testValue = "1->5 & 2->10";
            var expected = Runtime.NewRange(2, 5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAndRangeInterpreted2()
        {
            var testValue = "0->1 & 5->10";
            var expected = Runtime.Void;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Or Tests

        [TestMethod]
        public void TestOrRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(2, 10);
            var expected = Runtime.NewRange(1, 10);

            var result = value1 | value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestOrRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(5,10);
            var expected = Runtime.Void;

            var result = value1 | value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestOrRangeInterpreted1()
        {
            var testValue = "1->2 | 2->10";
            var expected = Runtime.NewRange(1, 10);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestOrRangeInterpreted2()
        {
            var testValue = "0->1 | 5->10";
            var expected = Runtime.Void;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Xor Tests

        [TestMethod]
        public void TestXorRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 5);
            var value2 = Runtime.NewRange(2, 10);
            var expected = Runtime.NewSet(
                            Runtime.NewRange(1),
                            Runtime.NewRange(6,10)
                            );

            var result = value1 ^ value2;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestXorRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,1);
            var value2 = Runtime.NewRange(5,10);
            var expected = Runtime.NewSet(value1,value2);

            var result = value1 ^ value2;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestXorRangeInterpreted1()
        {
            var testValue = "1->5 >< 2->10";
            var expected = Runtime.NewSet(
                            Runtime.NewRange(1),
                            Runtime.NewRange(6, 10)
                            );

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestXorRangeInterpreted2()
        {
            var testValue = "0->1 >< 5->10";
            var expected = Runtime.NewSet(Runtime.NewRange(0, 1), Runtime.NewRange(5, 10));

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Power Tests

        [TestMethod]
        public void TestPowerRangeInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var value2 = Runtime.NewRange(2,10);
            var expected = Runtime.NewRange(1,1024);

            var result = MathValue.PowerOperator(value1, value2);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestPowerRangeInternal2()
        {
            var value1 = Runtime.NewRange(9,10);
            var value2 = Runtime.NewRange(.5,2);
            var expected = Runtime.NewRange(3, 100);

            var result = MathValue.PowerOperator(value1, value2);
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestPowerRangeInterpreted1()
        {
            var testValue = "1->2 ^ 2->10";
            var expected = Runtime.NewRange(1, 1024);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestPowerRangeInterpreted2()
        {
            var testValue = "9->10 ^ 0.5->2";
            var expected = Runtime.NewRange(3, 100);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Equals Tests

        [TestMethod]
        public void TestEqualsRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestEqualsRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestEqualsRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 == value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }


        [TestMethod]
        public void TestEqualsRangeInterpreted1()
        {
            var testValue = "1->2 == 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestEqualsRangeInterpreted2()
        {
            var testValue = "1->2 == 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestEqualsRangeInterpreted3()
        {
            var testValue = "3->5 == 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        #endregion

        #region NotEquals Tests

        [TestMethod]
        public void TestNotEqualsRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestNotEqualsRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 != value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }


        [TestMethod]
        public void TestNotEqualsRangeInterpreted1()
        {
            var testValue = "1->2 <> 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestNotEqualsRangeInterpreted2()
        {
            var testValue = "1->2 <> 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestNotEqualsRangeInterpreted3()
        {
            var testValue = "3->5 <> 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region GreaterThan Tests

        [TestMethod]
        public void TestGreaterThanRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 > value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }


        [TestMethod]
        public void TestGreaterThanRangeInterpreted1()
        {
            var testValue = "1->2 > 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanRangeInterpreted2()
        {
            var testValue = "1->2 > 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanRangeInterpreted3()
        {
            var testValue = "3->5 > 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region GreaterThanOrEquals Tests

        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 >= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }


        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInterpreted1()
        {
            var testValue = "1->2 >= 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInterpreted2()
        {
            var testValue = "1->2 >= 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestGreaterThanOrEqualsRangeInterpreted3()
        {
            var testValue = "3->5 >= 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        #endregion

        #region LessThan Tests

        [TestMethod]
        public void TestLessThanRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 < value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }


        [TestMethod]
        public void TestLessThanRangeInterpreted1()
        {
            var testValue = "1->2 < 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanRangeInterpreted2()
        {
            var testValue = "1->2 < 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestLessThanRangeInterpreted3()
        {
            var testValue = "3->5 < 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        #endregion

        #region LessThanOrEquals Tests

        [TestMethod]
        public void TestLessThanOrEqualsRangeInternal1()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(3, 5);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsRangeInternal2()
        {
            var value1 = Runtime.NewRange(1, 2);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsRangeInternal3()
        {
            var value1 = Runtime.NewRange(3, 5);
            var value2 = Runtime.NewRange(1, 2);

            var result = value1 <= value2;
            var value = (bool)result.Value;
            Assert.IsFalse(value);
        }


        [TestMethod]
        public void TestLessThanOrEqualsRangeInterpreted1()
        {
            var testValue = "1->2 <= 3->5";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsRangeInterpreted2()
        {
            var testValue = "1->2 <= 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestLessThanOrEqualsRangeInterpreted3()
        {
            var testValue = "3->5 <= 1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        #endregion

        #endregion

        #region Unary Functions

        #region Negative Tests

        [TestMethod]
        public void TestNegativeRangeInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.NewRange(-1, -2);
            
            var result = -value1 ;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNegativeRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = Runtime.NewRange(-0, -10);

            var result = -value1;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestNegativeRangeInterpreted1()
        {
            var testValue = "-(1->2)";
            var expected = Runtime.NewRange(-1, -2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNegativeRangeInterpreted2()
        {
            var testValue = "-(0->10)";
            var expected = Runtime.NewRange(-0, -10);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Not Tests

        [TestMethod]
        public void TestNotRangeInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.False;
            
            var result = !value1 ;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNotRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,0);
            var expected = Runtime.True;

            var result = !value1;
            Assert.AreEqual(expected, result );
        }

        [TestMethod]
        public void TestNotRangeInterpreted1()
        {
            var testValue = "~(1->2)";
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestNotRangeInterpreted2()
        {
            var testValue = "~(0->0)";
            var expected = Runtime.True;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);

        }

        #endregion

        #endregion

        #region Conversion Functions

        #region Bool Functions

        [TestMethod]
        public void TestConvertRangeToBoolInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.True;

            var result = value1.ConvertToBoolean();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToBoolInternal2()
        {
            var value1 = Runtime.NewRange(0,0);
            var expected = Runtime.False;

            var result = value1.ConvertToBoolean();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToBoolInterpreted1()
        {
            var testValue = "tobool(1,2)";
            var expected =Runtime.True;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToBoolInterpreted2()
        {
            var testValue = "tobool(0,0)";
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Complex Functions

        [TestMethod]
        public void TestConvertRangeToComplexInternal1()
        {
            var value1 = Runtime.NewRange(3,4);
            var expected = Runtime.NewComplex(3.5,0);

            var result = value1.ConvertToComplex();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToComplexInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = Runtime.NewComplex(50,0);

            var result = value1.ConvertToComplex();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToComplexInterpreted1()
        {
            var testValue = "tocomplex(1->2)";
            var expected = Runtime.NewComplex(.5,0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToComplexInterpreted2()
        {
            var testValue = "tocomplex(0->10)";
            var expected = Runtime.NewComplex(50, 0); ;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Number Functions

        [TestMethod]
        public void TestConvertRangeToNumberInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.NewNumber(1.5);

            var result = value1.ConvertToNumber();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToNumberInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = Runtime.NewNumber(50);

            var result = value1.ConvertToNumber();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToNumberInterpreted1()
        {
            var testValue = "tonumber(1->2)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result.Value);
        }

        [TestMethod]
        public void TestConvertRangeToNumberInterpreted2()
        {
            var testValue = "tonumber(0->10)";
            var expected = 50.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result.Value);
        }

        #endregion

        #region Range Functions

        [TestMethod]
        public void TestConvertRangeToRangeInternal1()
        {
            var value1 = Runtime.NewRange(1,2);

            var result = value1.ConvertToRange();

            Assert.AreEqual(value1, result);

        }

        [TestMethod]
        public void TestConvertRangeToRangeInternal2()
        {
            var value1 = Runtime.NewRange(0,10);

            var result = value1.ConvertToRange();

            Assert.AreEqual(value1, result);

        }

        [TestMethod]
        public void TestConvertRangeToRangeInterpreted1()
        {
            var testValue = "torange(1,2)";
            var expected = Runtime.NewRange(1, 2);

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToRangeInterpreted2()
        {
            var testValue = "torange(0,10)";
            var expected = Runtime.NewRange(0, 10); ;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        #endregion

        #region Set Functions

        [TestMethod]
        public void TestConvertRangeToSetInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.NewSet(Runtime.NewRange(1, 2)); 

            var result = value1.ConvertToSet();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToSetInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = Runtime.NewSet(Runtime.NewRange(0, 10)); 

            var result = value1.ConvertToSet();

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToSetInterpreted1()
        {
            var testValue = "toset(1->2)";
            var expected = Runtime.NewSet(Runtime.NewRange(1,2));

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToSetInterpreted2()
        {
            var testValue = "toset(0->10)";
            var expected = Runtime.NewSet(Runtime.NewRange(0, 10));

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        #endregion

        #region String Functions

        [TestMethod]
        public void TestConvertRangeToStringInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = "1->2";

            var result = value1.ConvertToString();

            Assert.AreEqual(expected, (string)result.Value);
        }

        [TestMethod]
        public void TestConvertRangeToStringInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = "0->10";

            var result = value1.ConvertToString();

            Assert.AreEqual(expected, (string)result.Value);
        }

        [TestMethod]
        public void TestConvertRangeToStringInterpreted1()
        {
            var testValue = "tostring(1->2)";
            var expected = "1->2";

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (string)result.Value);
        }

        [TestMethod]
        public void TestConvertRangeToStringInterpreted2()
        {
            var testValue = "tostring(0->10)";
            var expected = "0->10";

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (string)result.Value);
        }

        #endregion

        #region Unit Functions

        [TestMethod]
        public void TestConvertRangeToUnitInternal1()
        {
            var value1 = Runtime.NewRange(1,2);
            var expected = Runtime.NewUnit(1.5,UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);

            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestConvertRangeToUnitInternal2()
        {
            var value1 = Runtime.NewRange(0,10);
            var expected = Runtime.NewUnit(50,UnitMeasure.Meters);

            var result = value1.ConvertToUnit(UnitMeasure.Meters);

            Assert.AreEqual(expected, result);

        }

        [TestMethod]
        public void TestConvertRangeToUnitInterpreted1()
        {
            var testValue = "tounit(1->2,'m')";
            var expected = Runtime.NewUnit(3, UnitMeasure.Meters);

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestConvertRangeToUnitInterpreted2()
        {
            var testValue = "tounit(0->10,'m')";
            var expected = Runtime.NewUnit(50, UnitMeasure.Meters);

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        #endregion

        #endregion
        
    }
}
