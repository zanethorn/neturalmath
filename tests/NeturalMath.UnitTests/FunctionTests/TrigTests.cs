﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class TrigTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Sin tests

        #region Tests for Sin

        [TestMethod]
        public void TestSinValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = 0.909297427;

            // Call the method
            var result = Runtime.SystemFunctions.Sin(testValue);


            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestSinValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -0.997494987;

            // Call the method
            var result = Runtime.SystemFunctions.Sin(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestSinValueInterpret1()
        {
            var testValue = "sin(2)";
            var expected = 0.909297427;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestSinValueInterpret2()
        {
            var testValue = "sin(-1.5)";
            var expected = -0.997494987;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestSinFunctionDef()
        {
            var functionName = "sin";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Asin

        [TestMethod]
        public void TestAsinValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = 1.57079632679;

            // Call the method
            var result = Runtime.SystemFunctions.Asin(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAsinValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5707963267948966192;

            // Call the method
            var result = Runtime.SystemFunctions.Asin(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAsinValueInterpret1()
        {
            var testValue = "asin(2)";
            var expected = 1.57079632679;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAsinValueInterpret2()
        {
            var testValue = "asin(-1.5)";
            var expected = -1.5707963267948966192;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAsinFunctionDef()
        {
            var functionName = "asin";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "angle";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Sinh

        [TestMethod]
        public void TestSinhValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = 3.626860408;

            // Call the method
            var result = Runtime.SystemFunctions.Sinh(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestSinhValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -2.129279455;

            // Call the method
            var result = Runtime.SystemFunctions.Sinh(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-7);
        }

        [TestMethod]
        public void TestSinhValueInterpret1()
        {
            var testValue = "sinh(2)";
            var expected = 3.626860408;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestSinhValueInterpret2()
        {
            var testValue = "sinh(-1.5)";
            var expected = -2.129279455;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-7);
        }

        [TestMethod]
        public void TestSinhFunctionDef()
        {
            var functionName = "sinh";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #endregion

        #region Cos tests

        #region Tests for Cos

        [TestMethod]
        public void TestCosValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = -.4161468365;

            // Call the method
            var result = Runtime.SystemFunctions.Cos(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestCosValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = .0707372017;

            // Call the method
            var result = Runtime.SystemFunctions.Cos(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestCosValueInterpret1()
        {
            var testValue = "cos(2)";
            var expected = -.4161468365;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestCosValueInterpret2()
        {
            var testValue = "cos(-1.5)";
            var expected = .0707372017;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestCosFunctionDef()
        {
            var functionName = "cos";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Acos

        [TestMethod]
        public void TestAcosValueInternal1()
        {
            var testValue = Runtime.NewNumber(.5);
            var expected = 1.047197551;

            // Call the method
            var result = Runtime.SystemFunctions.Acos(testValue);

            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestAcosValueInternal2()
        {
            var testValue = Runtime.NewNumber(-.38);
            var expected = 1.960592623;

            // Call the method
            var result = Runtime.SystemFunctions.Acos(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAcosValueInternal3()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = Runtime.Undefined; ;

            // Call the method
            var result = Runtime.SystemFunctions.Acos(testValue);


            Assert.AreEqual(expected,result);
        }

        [TestMethod]
        public void TestAcosValueInterpret1()
        {
            var testValue = "acos(0.5)";
            var expected = 1.047197551;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestAcosValueInterpret2()
        {
            var testValue = "acos(-0.38)";
            var expected = 1.960592623;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAcosValueInterpret3()
        {
            var testValue = "acos(2)";
            var expected = Runtime.Undefined;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestAcosFunctionDef()
        {
            var functionName = "acos";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "angle";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion


        #region Tests for Cosh

        [TestMethod]
        public void TestCoshValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = 3.762195691;

            // Call the method
            var result = Runtime.SystemFunctions.Cosh(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestCoshValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 2.352409615;

            // Call the method
            var result = Runtime.SystemFunctions.Cosh(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestCoshValueInterpret1()
        {
            var testValue = "cosh(2)";
            var expected = 3.762195691;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestCoshValueInterpret2()
        {
            var testValue = "cosh(-1.5)";
            var expected = 2.352409615;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestCoshFunctionDef()
        {
            var functionName = "cosh";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #endregion

        #region Tan tests

        #region Tests for Tan

        [TestMethod]
        public void TestTanValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = -2.185039863;

            // Call the method
            var result = Runtime.SystemFunctions.Tan(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -14.10141995;

            // Call the method
            var result = Runtime.SystemFunctions.Tan(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanValueInterpret1()
        {
            var testValue = "tan(2)";
            var expected = -2.185039863;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanValueInterpret2()
        {
            var testValue = "tan(-1.5)";
            var expected = -14.10141995;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanFunctionDef()
        {
            var functionName = "tan";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Atan

        [TestMethod]
        public void TestAtanValueInternal1()
        {
            var testValue = Runtime.NewNumber(2);
            var expected = 1.1071487177;

            // Call the method
            var result = Runtime.SystemFunctions.Atan(testValue);
            

            Assert.AreEqual(expected, (double)result.Value,1e-8);
        }

        [TestMethod]
        public void TestAtanValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -0.9827937232;

            // Call the method
            var result = Runtime.SystemFunctions.Atan(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestAtanValueInterpret1()
        {
            var testValue = "atan(2)";
            var expected = 1.1071487177;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestAtanValueInterpret2()
        {
            var testValue = "atan(-1.5)";
            var expected = -0.9827937232;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestAtanFunctionDef()
        {
            var functionName = "atan";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "angle";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Tanh

        [TestMethod]
        public void TestTanhValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 1.0;

            // Call the method
            var result = Runtime.SystemFunctions.Tanh(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanhValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -.9051482536;

            // Call the method
            var result = Runtime.SystemFunctions.Tanh(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanhValueInterpret1()
        {
            var testValue = "tanh(30)";
            var expected = 1.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanhValueInterpret2()
        {
            var testValue = "tanh(-1.5)";
            var expected = -.9051482536;

            // Call the method
            var result = Runtime.ExecuteText(testValue);


            Assert.AreEqual(expected, (double)result.Value, 1e-8);
        }

        [TestMethod]
        public void TestTanhFunctionDef()
        {
            var functionName = "tanh";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "radians";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #endregion

    }
}
