﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NeturalMath.Units;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class TypeFunctionTests
    {

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Tests for IsFalse

        [TestMethod]
        public void TestIsFalseValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);


            // Call the method
            var result = Runtime.SystemFunctions.IsFalse(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse( value);
        }

        [TestMethod]
        public void TestIsFalseValueInternal2()
        {
            var testValue = Runtime.NewBool(false);

            // Call the method
            var result = Runtime.SystemFunctions.IsFalse(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsFalseValueInterpret1()
        {
            var testValue = "isfalse(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsFalseValueInterpret2()
        {
            var testValue = "isfalse(false)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsFalseFunctionDef()
        {
            var functionName = "isfalse";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsTrue

        [TestMethod]
        public void TestIsTrueValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsTrue(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsTrueValueInternal2()
        {
            var testValue = Runtime.NewBool(false);

            // Call the method
            var result = Runtime.SystemFunctions.IsTrue(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsTrueValueInterpret1()
        {
            var testValue = "istrue(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        public void TestIsTrueValueInterpret2()
        {
            var testValue = "istrue(false)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsTrueFunctionDef()
        {
            var functionName = "istrue";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsImaginary

        [TestMethod]
        public void TestIsImaginaryValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsImaginary(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsImaginaryValueInternal2()
        {
            var testValue = Runtime.NewComplex(-1.5,12);

            // Call the method
            var result = Runtime.SystemFunctions.IsImaginary(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsImaginaryValueInterpret1()
        {
            var testValue = "isimaginary(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsImaginaryValueInterpret2()
        {
            var testValue = "isimaginary((-1.5,12i))";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsImaginaryFunctionDef()
        {
            var functionName = "isimaginary";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

        }

        #endregion

        #region Tests for IsPositive

        [TestMethod]
        public void TestIsPositiveValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.IsPositive(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsPositiveValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.IsPositive(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsPositiveValueInterpret1()
        {
            var testValue = "ispositive(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestIsPositiveValueInterpret2()
        {
            var testValue = "ispositive(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsPositiveFunctionDef()
        {
            var functionName = "ispositive";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsNegative

        [TestMethod]
        public void TestIsNegativeValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.IsNegative(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsNegativeValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.IsNegative(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsNegativeValueInterpret1()
        {
            var testValue = "isnegative(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestIsNegativeValueInterpret2()
        {
            var testValue = "isnegative(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsNegativeFunctionDef()
        {
            var functionName = "isnegative";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsVoid

        [TestMethod]
        public void TestIsVoidValueInternal1()
        {
            var testValue = Runtime.Void;

            // Call the method
            var result = Runtime.SystemFunctions.IsVoid(testValue);

            Assert.AreEqual(Runtime.True, result);
        }

        [TestMethod]
        public void TestIsVoidValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);

            // Call the method
            var result = Runtime.SystemFunctions.IsVoid(testValue);

            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestIsVoidValueInterpret1()
        {
            var testValue = "isvoid(void)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(Runtime.True, result);
        }

        public void TestIsVoidValueInterpret2()
        {
            var testValue = "isvoid(-1.5)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(Runtime.False, result);
        }

        [TestMethod]
        public void TestIsVoidFunctionDef()
        {
            var functionName = "isvoid";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Type

        [TestMethod]
        public void TestTypeValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.Type(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestTypeValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Type(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestTypeValueInterpret1()
        {
            var testValue = "type(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestTypeValueInterpret2()
        {
            var testValue = "type(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestTypeFunctionDef()
        {
            var functionName = "type";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion


        #region Conversion Tests


        #region Tests for ToBool

        [TestMethod]
        public void TestToBoolValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.ToBool(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestToBoolValueInternal2()
        {
            var testValue = Runtime.NewNumber(0);

            // Call the method
            var result = Runtime.SystemFunctions.ToBool(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestToBoolValueInterpret1()
        {
            var testValue = "tobool(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        public void TestToBoolValueInterpret2()
        {
            var testValue = "tobool(0)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestToBoolFunctionDef()
        {
            var functionName = "tobool";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsBool

        [TestMethod]
        public void TestIsBoolValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.SystemFunctions.IsBool(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestIsBoolValueInternal2()
        {
            var testValue = Runtime.False;
            var expected = Runtime.True;

            // Call the method
            var result = Runtime.SystemFunctions.IsBool(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestIsBoolValueInterpret1()
        {
            var testValue = "isbool(30)";
            var expected = Runtime.False;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        public void TestIsBoolValueInterpret2()
        {
            var testValue = "isbool(false)";
            var expected = Runtime.True;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestIsBoolFunctionDef()
        {
            var functionName = "isbool";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for ToComplex

        [TestMethod]
        public void TestToComplexValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = Runtime.NewComplex(30, 0);

            // Call the method
            var result = Runtime.SystemFunctions.ToComplex(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToComplexValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewComplex(-1.5, 0);

            // Call the method
            var result = Runtime.SystemFunctions.ToComplex(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToComplexValueInterpret1()
        {
            var testValue = "tocomplex(30)";
            var expected = Runtime.NewComplex(30,0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        public void TestToComplexValueInterpret2()
        {
            var testValue = "tocomplex(-1.5)";
            var expected = Runtime.NewComplex(-1.5,0);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToComplexFunctionDef()
        {
            var functionName = "tocomplex";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsComplex

        [TestMethod]
        public void TestIsComplexValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.IsComplex(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsComplexValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.IsComplex(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsComplexValueInterpret1()
        {
            var testValue = "iscomplex(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestIsComplexValueInterpret2()
        {
            var testValue = "iscomplex(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsComplexFunctionDef()
        {
            var functionName = "iscomplex";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for ToNumber

        [TestMethod]
        public void TestToNumberValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.ToNumber(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestToNumberValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            // Call the method
            var result = Runtime.SystemFunctions.ToNumber(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestToNumberValueInterpret1()
        {
            var testValue = "tonumber(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestToNumberValueInterpret2()
        {
            var testValue = "tonumber(-1.5)";
            var expected = -1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestToNumberFunctionDef()
        {
            var functionName = "tonumber";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsNumber

        [TestMethod]
        public void TestIsNumberValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsNumber(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsNumberValueInternal2()
        {
            var testValue = Runtime.NewString("abcd");

            // Call the method
            var result = Runtime.SystemFunctions.IsNumber(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsNumberValueInterpret1()
        {
            var testValue = "isnumber(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        public void TestIsNumberValueInterpret2()
        {
            var testValue = "isnumber('abcd')";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsNumberFunctionDef()
        {
            var functionName = "isnumber";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for ToRange

        [TestMethod]
        public void TestToRangeValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = Runtime.NewRange(30,30);

            // Call the method
            var result = Runtime.SystemFunctions.ToRange(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToRangeValueInternal2()
        {
            var lower = Runtime.NewNumber(-1.5);
            var upper = Runtime.NewNumber(1.5);
            var expected = Runtime.NewRange(-1.5, 1.5);

            // Call the method
            var result = Runtime.SystemFunctions.ToRange(lower,upper);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToRangeValueInterpret1()
        {
            var testValue = "torange(30)";
            var expected = Runtime.NewRange(30, 30);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        public void TestToRangeValueInterpret2()
        {
            var testValue = "torange(-1.5,1.5)";
            var expected = Runtime.NewRange(-1.5, 1.5);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToRangeFunctionDef()
        {
            var functionName = "torange";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "lower";
            var param1Value = Runtime.Void;

            var param2 = function.Parameters[1];
            var param2Name = "upper";
            var param2Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

            Assert.AreEqual(param2Name, param2.Name);
            Assert.AreEqual(param2Value, param2.Value.Execute());
        }

        #endregion

        #region Tests for IsRange

        [TestMethod]
        public void TestIsRangeValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsRange(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsRangeValueInternal2()
        {
            var testValue = Runtime.NewRange(-1.5,2);

            // Call the method
            var result = Runtime.SystemFunctions.IsRange(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsRangeValueInterpret1()
        {
            var testValue = "isrange(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsRangeValueInterpret2()
        {
            var testValue = "isrange(-1.5->2)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsRangeFunctionDef()
        {
            var functionName = "isrange";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

        }

        #endregion

        #region Tests for ToSet

        [TestMethod]
        public void TestToSetValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = (SetValue)Runtime.SystemFunctions.ToSet(testValue);
            var value1 = result.Single();

            Assert.AreEqual(testValue, value1);
        }

        [TestMethod]
        public void TestToSetValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);

            // Call the method
            var result = (SetValue)Runtime.SystemFunctions.ToSet(testValue);
            var value1 = result.Single();

            Assert.AreEqual(testValue, value1);
        }

        [TestMethod]
        public void TestToSetValueInterpret1()
        {
            var testValue = "toset(30)";

            // Call the method
            var result = (SetValue)Runtime.ExecuteText(testValue);
            var value1 = result.Single();

            Assert.AreEqual(Runtime.NewNumber(30), value1);
        }

        public void TestToSetValueInterpret2()
        {
            var testValue = "toset(-1.5)";

            // Call the method
            var result = (SetValue)Runtime.ExecuteText(testValue);
            var value1 = result.Single();

            Assert.AreEqual(Runtime.NewNumber(-1.5), value1);
        }

        [TestMethod]
        public void TestToSetFunctionDef()
        {
            var functionName = "toset";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

        }

        #endregion

        #region Tests for IsSet

        [TestMethod]
        public void TestIsSetValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsSet(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsSetValueInternal2()
        {
            var testValue = Runtime.NewSet(Runtime.NewNumber(-1.5));

            // Call the method
            var result = Runtime.SystemFunctions.IsSet(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsSetValueInterpret1()
        {
            var testValue = "isset(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsSetValueInterpret2()
        {
            var testValue = "isset([-1.5])";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsSetFunctionDef()
        {
            var functionName = "isset";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for ToString

        [TestMethod]
        public void TestToStringValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = Runtime.NewString("30");

            // Call the method
            var result = Runtime.SystemFunctions.ToString(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToStringValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewString("-1.5");

            // Call the method
            var result = Runtime.SystemFunctions.ToString(testValue);

            Assert.AreEqual(expected,result);
        }

        [TestMethod]
        public void TestToStringValueInterpret1()
        {
            var testValue = "tostring(30)";
            var expected = Runtime.NewString("30");

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        public void TestToStringValueInterpret2()
        {
            var testValue = "tostring(-1.5)";
            var expected = Runtime.NewString("-1.5");

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToStringFunctionDef()
        {
            var functionName = "tostring";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsString

        [TestMethod]
        public void TestIsStringValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsString(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsStringValueInternal2()
        {
            var testValue = Runtime.NewString("-1.5");

            // Call the method
            var result = Runtime.SystemFunctions.IsString(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsStringValueInterpret1()
        {
            var testValue = "isstring(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsStringValueInterpret2()
        {
            var testValue = "isstring('-1.5')";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsStringFunctionDef()
        {
            var functionName = "isstring";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

        }

        #endregion

        #region Tests for ToUnit

        [TestMethod]
        public void TestToUnitValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = Runtime.NewUnit(30,UnitMeasure.Meters);

            // Call the method
            var result = Runtime.SystemFunctions.ToUnit(testValue,UnitMeasure.Meters);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToUnitValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewUnit(-1.5, UnitMeasure.Meters);

            // Call the method
            var result = Runtime.SystemFunctions.ToUnit(testValue, UnitMeasure.Meters);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToUnitValueInterpret1()
        {
            var testValue = "tounit(30,#m)";
            var expected = Runtime.NewUnit(30, UnitMeasure.Meters); ;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        public void TestToUnitValueInterpret2()
        {
            var testValue = "tounit(-1.5,#m)";
            var expected = Runtime.NewUnit(-1.5, UnitMeasure.Meters);

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestToUnitFunctionDef()
        {
            var functionName = "tounit";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsUnit

        [TestMethod]
        public void TestIsUnitValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);

            // Call the method
            var result = Runtime.SystemFunctions.IsUnit(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        [TestMethod]
        public void TestIsUnitValueInternal2()
        {
            var testValue = Runtime.NewUnit(-1.5,UnitMeasure.Meters);

            // Call the method
            var result = Runtime.SystemFunctions.IsUnit(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsUnitValueInterpret1()
        {
            var testValue = "isunit(30)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsFalse(value);
        }

        public void TestIsUnitValueInterpret2()
        {
            var testValue = "isunit(-1.5#m)";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (bool)result.Value;

            Assert.IsTrue(value);
        }

        [TestMethod]
        public void TestIsUnitFunctionDef()
        {
            var functionName = "isunit";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #endregion
    }
}
