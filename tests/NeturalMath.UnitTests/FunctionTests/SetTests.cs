﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class SetTests
    {

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Tests for Avg

        [TestMethod]
        public void TestAvgValueInternal1()
        {
            var testValue = Runtime.NewSet(
                Runtime.NewNumber(30),
                Runtime.NewNumber(35),
                Runtime.NewNumber(16),
                Runtime.NewNumber(9),
                Runtime.NewNumber(10)
                );
            var expected = 20.0;

            // Call the method
            var result = Runtime.SystemFunctions.Avg(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAvgValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Avg(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAvgValueInterpret1()
        {
            var testValue = "avg([30,35,16,9,10])";
            var expected = 20.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestAvgValueInterpret2()
        {
            var testValue = "avg(-1.5)";
            var expected = -1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAvgFunctionDef()
        {
            var functionName = "avg";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Max

        [TestMethod]
        public void TestMaxValueInternal1()
        {
            var testValue = Runtime.NewSet(
                Runtime.NewNumber(30),
                Runtime.NewNumber(35),
                Runtime.NewNumber(16),
                Runtime.NewNumber(9),
                Runtime.NewNumber(10)
                );
            var expected = 35.0;

            // Call the method
            var result = Runtime.SystemFunctions.Max(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMaxValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Max(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestMaxValueInterpret1()
        {
            var testValue = "max([30,35,16,9,10])";
            var expected = 35.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestMaxValueInterpret2()
        {
            var testValue = "max(-1.5)";
            var expected = -1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMaxFunctionDef()
        {
            var functionName = "max";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Min

        [TestMethod]
        public void TestMinValueInternal1()
        {
            var testValue = Runtime.NewSet(
                Runtime.NewNumber(30),
                Runtime.NewNumber(35),
                Runtime.NewNumber(16),
                Runtime.NewNumber(9),
                Runtime.NewNumber(10)
                );
            var expected = 9.0;

            // Call the method
            var result = Runtime.SystemFunctions.Min(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMinValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Min(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMinValueInterpret1()
        {
            var testValue = "min([30,35,16,9,10])";
            var expected = 9.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestMinValueInterpret2()
        {
            var testValue = "min(-1.5)";
            var expected = -1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMinFunctionDef()
        {
            var functionName = "min";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Median

        [TestMethod]
        public void TestMedianValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.Median(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestMedianValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Median(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestMedianValueInterpret1()
        {
            var testValue = "median(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestMedianValueInterpret2()
        {
            var testValue = "median(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestMedianFunctionDef()
        {
            var functionName = "median";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Mean

        [TestMethod]
        public void TestMeanValueInternal1()
        {
            var testValue = Runtime.NewSet(
                Runtime.NewNumber(30),
                Runtime.NewNumber(35),
                Runtime.NewNumber(16),
                Runtime.NewNumber(9),
                Runtime.NewNumber(10)
                );
            var expected = 20.0;

            // Call the method
            var result = Runtime.SystemFunctions.Mean(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMeanValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Mean(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMeanValueInterpret1()
        {
            var testValue = "mean([30,35,16,9,10])";
            var expected = 20.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestMeanValueInterpret2()
        {
            var testValue = "mean(-1.5)";
            var expected = -1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestMeanFunctionDef()
        {
            var functionName = "mean";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Mode

        [TestMethod]
        public void TestModeValueInternal1()
        {
            var testValue = Runtime.NewSet(
                Runtime.NewNumber(30),
                Runtime.NewNumber(35),
                Runtime.NewNumber(10),
                Runtime.NewNumber(9),
                Runtime.NewNumber(10)
                );
            var expected = 10.0;

            // Call the method
            var result = Runtime.SystemFunctions.Mode(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestModeValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Mode(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestModeValueInterpret1()
        {
            var testValue = "mode([30,35,10,9,10])";
            var expected = 10.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestModeValueInterpret2()
        {
            var testValue = "mode(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestModeFunctionDef()
        {
            var functionName = "mode";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for StdDev

        [TestMethod]
        public void TestStdDevValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.StdDev(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestStdDevValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.StdDev(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestStdDevValueInterpret1()
        {
            var testValue = "stddev(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        public void TestStdDevValueInterpret2()
        {
            var testValue = "stddev(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestStdDevFunctionDef()
        {
            var functionName = "stddev";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "set";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

    }
}
