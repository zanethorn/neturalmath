﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class StringTests
    {

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Tests for Trim

        [TestMethod]
        public void TestTrimValueInternal1()
        {
            var testValue = Runtime.NewString("  has space    ");
            var expected = "has space";

            // Call the method
            var result = Runtime.SystemFunctions.Trim(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestTrimValueInternal2()
        {
            var testValue = Runtime.NewString("  has spaces");
            var expected = "has spaces";

            // Call the method
            var result = Runtime.SystemFunctions.Trim(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestTrimValueInterpret1()
        {
            var testValue = "trim(' has space')";
            var expected = "has space";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        public void TestTrimValueInterpret2()
        {
            var testValue = "trim('  has space  ')";
            var expected = "has space";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestTrimFunctionDef()
        {
            var functionName = "trim";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Length

        [TestMethod]
        public void TestLengthValueInternal1()
        {
            var testValue = Runtime.NewString("12345");
            var expected = 5;

            // Call the method
            var result = Runtime.SystemFunctions.Length(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestLengthValueInternal2()
        {
            var testValue = Runtime.NewString("abcdefghijklmnopqrstuvwxyz");
            var expected = 26;

            // Call the method
            var result = Runtime.SystemFunctions.Length(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestLengthValueInterpret1()
        {
            var testValue = "length('12345')";
            var expected = 5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestLengthValueInterpret2()
        {
            var testValue = "length('abcdefghijklmnopqrstuvwxyz')";
            var expected = 26;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestLengthFunctionDef()
        {
            var functionName = "length";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Reverse

        [TestMethod]
        public void TestReverseValueInternal1()
        {
            var testValue = Runtime.NewString("12345");
            var expected = "54321";

            // Call the method
            var result = Runtime.SystemFunctions.Reverse(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestReverseValueInternal2()
        {
            var testValue = Runtime.NewString("this is a string");
            var expected = "gnirts a si siht";

            // Call the method
            var result = Runtime.SystemFunctions.Reverse(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestReverseValueInterpret1()
        {
            var testValue = "reverse('12345')";
            var expected = "54321";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        public void TestReverseValueInterpret2()
        {
            var testValue = "reverse('this is a string')";
            var expected = "gnirts a si siht";

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (string)result.Value;

            Assert.AreEqual(expected, value);
        }

        [TestMethod]
        public void TestReverseFunctionDef()
        {
            var functionName = "reverse";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

    }
}
