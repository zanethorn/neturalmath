﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class BasicMathFunctionsTest
    {

        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Tests for Abs

        [TestMethod]
        public void TestAbsValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.Abs(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAbsValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = 1.5;

            // Call the method
            var result = Runtime.SystemFunctions.Abs(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAbsValueInterpret1()
        {
            var testValue = "abs(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestAbsValueInterpret2()
        {
            var testValue = "abs(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestAbsFunctionDef()
        {
            var functionName = "abs";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Ceiling

        [TestMethod]
        public void TestCeilingValueInternal1()
        {
            var testValue = Runtime.NewNumber(30.5);
            var expected = 31.0;

            // Call the method
            var result = Runtime.SystemFunctions.Ceiling(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestCeilingValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -1;

            // Call the method
            var result = Runtime.SystemFunctions.Ceiling(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestCeilingValueInterpret1()
        {
            var testValue = "ceiling(30.5)";
            var expected = 31.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestCeilingValueInterpret2()
        {
            var testValue = "ceiling(-1.5)";
            var expected = 1.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestCeilingFunctionDef()
        {
            var functionName = "ceiling";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Floor

        [TestMethod]
        public void TestFloorValueInternal1()
        {
            var testValue = Runtime.NewNumber(30.5);
            var expected = 30.0;

            // Call the method
            var result = Runtime.SystemFunctions.Floor(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestFloorValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = -2;

            // Call the method
            var result = Runtime.SystemFunctions.Floor(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestFloorValueInterpret1()
        {
            var testValue = "floor(30.5)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestFloorValueInterpret2()
        {
            var testValue = "floor(-1.5)";
            var expected = -2;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestFloorFunctionDef()
        {
            var functionName = "floor";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Log

        [TestMethod]
        public void TestLogValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 1.477121255;

            // Call the method
            var result = Runtime.SystemFunctions.Log(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        [TestMethod]
        public void TestLogValueInternal2()
        {
            var testValue = Runtime.NewNumber(1.5);
            var expected = .1760912591;

            // Call the method
            var result = Runtime.SystemFunctions.Log(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        [TestMethod]
        public void TestLogValueInterpret1()
        {
            var testValue = "log(30)";
            var expected = 1.477121255;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        public void TestLogValueInterpret2()
        {
            var testValue = "log(-1.5)";
            var expected = .1760912591;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        [TestMethod]
        public void TestLogFunctionDef()
        {
            var functionName = "log";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            var param2 = function.Parameters[1];
            var param2Name = "basen";
            var param2Value = Runtime.NewNumber(10);

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

            Assert.AreEqual(param2Name, param2.Name);
            Assert.AreEqual(param2Value, param2.Value.Execute());
        }

        #endregion

        #region Tests for Ln

        [TestMethod]
        public void TestLnValueInternal1()
        {
            var testValue = Runtime.NewNumber(30);
            var expected = 3.401197382;

            // Call the method
            var result = Runtime.SystemFunctions.Ln(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        [TestMethod]
        public void TestLnValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewComplex(.4054651081,Math.PI);

            // Call the method
            var result = Runtime.SystemFunctions.Ln(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestLnValueInterpret1()
        {
            var testValue = "ln(30)";
            var expected = 3.401197382;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        public void TestLnValueInterpret2()
        {
            var testValue = "ln(200)";
            var expected = 5.298317367;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 1e-9);
        }

        [TestMethod]
        public void TestLnFunctionDef()
        {
            var functionName = "ln";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

        }

        #endregion

        #region Tests for Root

        [TestMethod]
        public void TestRootValueInternal1()
        {
            var testValue = Runtime.NewNumber(8);
            var basen = Runtime.NewNumber(3);
            var expected = 2.0;

            // Call the method
            var result = Runtime.SystemFunctions.Root(testValue,basen);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRootValueInternal2()
        {
            var testValue = Runtime.NewNumber(16);
            var expected = 4;

            // Call the method
            var result = Runtime.SystemFunctions.Root(testValue,Runtime.Void);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRootValueInterpret1()
        {
            var testValue = "root(8,3)";
            var expected = 2;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestRootValueInterpret2()
        {
            var testValue = "root(16)";
            var expected = 4;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRootFunctionDef()
        {
            var functionName = "root";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            var param2 = function.Parameters[1];
            var param2Name = "basen";
            var param2Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

            Assert.AreEqual(param2Name, param2.Name);
            Assert.AreEqual(param2Value, param2.Value.Execute());
        }

        #endregion

        #region Tests for Round

        [TestMethod]
        public void TestRoundValueInternal1()
        {
            var testValue = Runtime.NewNumber(30.12345);
            var places = Runtime.NewNumber(2);
            var expected = 30.12;

            // Call the method
            var result = Runtime.SystemFunctions.Round(testValue,places);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRoundValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var places = Runtime.Void;
            var expected = -2.0;

            // Call the method
            var result = Runtime.SystemFunctions.Round(testValue,places);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRoundValueInterpret1()
        {
            var testValue = "round(30.12345,2)";
            var expected = 30.12;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestRoundValueInterpret2()
        {
            var testValue = "round(-1.5)";
            var expected = -2.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestRoundFunctionDef()
        {
            var functionName = "round";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            var param2 = function.Parameters[1];
            var param2Name = "places";
            var param2Value = Runtime.Zero;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());

            Assert.AreEqual(param2Name, param2.Name);
            Assert.AreEqual(param2Value, param2.Value.Execute());
        }

        #endregion

        #region Tests for Sqrt

        [TestMethod]
        public void TestSqrtValueInternal1()
        {
            var testValue = Runtime.NewNumber(9);
            var expected = 3;

            // Call the method
            var result = Runtime.SystemFunctions.Sqrt(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        [TestMethod]
        public void TestSqrtValueInternal2()
        {
            var testValue = Runtime.NewNumber(-1.5);
            var expected = Runtime.NewComplex(0,1.22474487);

            // Call the method
            var result = Runtime.SystemFunctions.Sqrt(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSqrtValueInterpret1()
        {
            var testValue = "sqrt(30)";
            var expected = 5.477225575;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
        }

        public void TestSqrtValueInterpret2()
        {
            var testValue = "sqrt(-1.5)";
            var expected = Runtime.NewComplex(0, 1.22474487); ;

            // Call the method
            var result = Runtime.ExecuteText(testValue);

            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSqrtFunctionDef()
        {
            var functionName = "sqrt";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters[0];
            var param1Name = "value";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion



        


    }
}
