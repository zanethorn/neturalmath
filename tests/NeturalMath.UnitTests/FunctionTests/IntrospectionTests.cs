﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NeturalMath.UnitTests.FunctionTests
{
    [TestClass]
    public class IntrospectionTests
    {
        public MathRuntime Runtime { get; private set; }

        [TestInitialize]
        public void Initialize()
        {
            Runtime = new MathRuntime();
        }

        #region Tests for Domains


        [TestMethod]
        public void TestDomainsValueInterpret1()
        {
            var testValue = "domains(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestDomainsValueInterpret2()
        {
            var testValue = "domains(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestDomainsFunctionDef()
        {
            var functionName = "domains";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for DomainsL

        [TestMethod]
        public void TestDomainsLValueInterpret1()
        {
            var testValue = "domainsl(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestDomainsLValueInterpret2()
        {
            var testValue = "domainsl(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestDomainsLFunctionDef()
        {
            var functionName = "domainsl";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Functions

       

        [TestMethod]
        public void TestFunctionsValueInterpret1()
        {
            var testValue = "functions(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestFunctionsValueInterpret2()
        {
            var testValue = "functions(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestFunctionsFunctionDef()
        {
            var functionName = "functions";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for FunctionsL


        [TestMethod]
        public void TestFunctionsLValueInterpret1()
        {
            var testValue = "functionsl(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestFunctionsLValueInterpret2()
        {
            var testValue = "functionsl(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestFunctionsLFunctionDef()
        {
            var functionName = "functionsl";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for IsDef


        [TestMethod]
        public void TestIsDefValueInterpret1()
        {
            var testValue = "isdef(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsDefValueInterpret2()
        {
            var testValue = "isdef(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestIsDefFunctionDef()
        {
            var functionName = "isdef";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for Variables


        [TestMethod]
        public void TestVariablesValueInterpret1()
        {
            var testValue = 
@"test = {
    public a = void
    b = void
    global c = void
    private d = void
    }

variables($test)
";
            // Call the method
            var result = (SetValue)Runtime.ExecuteText(testValue);
            var resultSet = result.Results.Select(r=>(MathSymbol)r.Value).ToArray();

            // get concrete symbols
            var testDomain = (Domain)Runtime.CurrentDomain.GetSymbol("test");

            var a = testDomain.GetSymbol("a");
            var b = testDomain.GetSymbol("b");
            var c = testDomain.GetSymbol("c");
            var d = testDomain.GetSymbol("d");

            Assert.IsTrue(resultSet.Contains(a));
            Assert.IsTrue(resultSet.Contains(b));
            Assert.IsTrue(resultSet.Contains(c));
            Assert.IsTrue(resultSet.Contains(d));
        }

        [TestMethod]
        public void TestVariablesValueInterpret2()
        {
            var testValue = "variables(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestVariablesFunctionDef()
        {
            var functionName = "variables";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

        #region Tests for VariablesL


        [TestMethod]
        public void TestVariablesLValueInterpret1()
        {
            var testValue = "variablesl(30)";
            var expected = 30.0;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestVariablesLValueInterpret2()
        {
            var testValue = "variablesl(-1.5)";
            var expected = 1.5;

            // Call the method
            var result = Runtime.ExecuteText(testValue);
            var value = (double)result.Value;

            Assert.AreEqual(expected, value, 0.0000000001);
            Assert.Inconclusive("This unit test is not finished!");
        }

        [TestMethod]
        public void TestVariablesLFunctionDef()
        {
            var functionName = "variablesl";
            var function = Runtime.RootDomain.GetFunction(functionName);

            var param1 = function.Parameters.ElementAt(0);
            var param1Name = "symbol";
            var param1Value = Runtime.Void;

            // Test function definition
            Assert.AreEqual(functionName, function.Name);

            // Test parameters
            Assert.AreEqual(param1Name, param1.Name);
            Assert.AreEqual(param1Value, param1.Value.Execute());
        }

        #endregion

    }
}
