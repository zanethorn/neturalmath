﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

namespace NeturalMath
{
    /// <summary>
    /// Holds a constant list of all internal NeturalMath error codes
    /// </summary>
    public enum ErrorCodes
    {
        #pragma warning disable 1591

        // Generic / Unknown Exceptions          0x,
        GenericError                           = 0000,
        ApplicationAbort                       = 0001,
        NotImplemented                         = 0002,
        InvalidOperation                       = 0003,

        // Usage Exceptions                      01xx
        ImmutableVariable                      = 0100,
        DuplicateVariable                      = 0101,
        ConstantModified                       = 0102,
        CannotDefineImplicitVariable           = 0103,
        CannotRedefineFunctionOrDomain         = 0104,
        CannotChangeAccessOnExistingMember     = 0105,
        MemberNotFound                         = 0106,
        DottedExpressionTargetNotDomain        = 0107,
        ExpectedVariable                       = 0108,
        CannotCloneParent                      = 0109,
        CannotCloneSelf                        = 0110,
        VariableHasNoValue                     = 0111,
        ExpectedFunctionOrDomain               = 0112,
        CannotInvokeDomainWithinItself         = 0113,
        InvalidParameterDefinition             = 0114,
        InvalidVariableAssignment              = 0115,
        CannotAccessPrivateOrLocalMember       = 0116,
        CannotModifyReadonlyVariable           = 0117,
        RangeCannotHaveUndefinedParameters     = 0118,

        // Lexical exceptions                    1x,
        GenericLexError                        = 1000,
        UnrecognizedCharacterSequence          = 1001,
        UnexpectedEndOfLine                    = 1002,
        UnexpectedEndOfFile                    = 1003,

        // Syntax exceptions                     11xx,
        GenericSyntaxException                 = 1100,
        UnrecognizedTokenType                  = 1101,
        ExpectedIndentifier                    = 1102,
        SymbolExpected                         = 1103,
        DomainExpected                         = 1104,
        VariableExpected                       = 1105,
        FunctionExpected                       = 1106,

        // <Reserved for Future Use>             2xxx,


        // Casting / conversion exceptions       3xxx,
        UnrecognizedType                       = 3000,
        StringCouldNotConvert                  = 3001,
        UnitDimensionMismatch                  = 3002,
        OperatorNotSupported                   = 3003,
        CouldNotCastToNumber                   = 3004,
        CouldNotLookupUnitDimension            = 3005,
        InvalidUnitOperator                    = 2006,
        UnitMagnitideNotConstant               = 2007,
        SetCouldNotConvertToScalar             = 2008,
        SetCouldNotConvertToRange              = 2009,
        SetCouldNotConvertToComplex            = 2010,

        // .NET Interop codes                    4xxx,


        // System Function Codes                 5xxx,
        NoDomainFound                          = 5001,
        TooManyParametersProvided              = 5002,

        //Mathematical Exceptions                Axxx,
        DivideByZero                           = 6000,

        #pragma warning restore 1591
    }
}
