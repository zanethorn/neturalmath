﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;

namespace NeturalMath
{
    /// <summary>
    /// Used with variables to hold their access types and scope modfiers
    /// </summary>
    [Flags]
    public enum AccessabilityLevels
    {
        /// <summary>
        /// Private access, the variable or function can only be accessed from the local domain
        /// </summary>
        Private = 0,

        /// <summary>
        /// Public access, the variable or function can be accessed using the domain name
        /// </summary>
        Public = 1,

        /// <summary>
        /// Global access, the variable or function can be accessed anywhere.
        /// </summary>
        Global = 2,

    }
}
