﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeturalMath.Expressions;

namespace NeturalMath.Units
{
    public class Measurements
    {
        private readonly Dictionary<string, UnitDimension> _knownDimensions = new Dictionary<string,UnitDimension>();
        private readonly UnitMeasureVisitor _visitor;

        internal Measurements(MathRuntime runtime)
        {
            Runtime = runtime;
            _visitor = new UnitMeasureVisitor(runtime);
        }

        public MathRuntime Runtime { get; private set; }

        public UnitDimension GetDimension(string symbol)
        {
            if (!_knownDimensions.ContainsKey(symbol))
                return null;
            return _knownDimensions[symbol];
        }

        public UnitMeasure CreateMeasure(ValueExpression expression)
        {
            return _visitor.Visit(expression);
        }

    }
}
