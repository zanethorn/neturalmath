﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Units
{
    /// <summary>
    /// Represents a kind of measurement
    /// </summary>
    public class UnitMeasure
    {
        #region Local Members

        /// <summary>
        /// Get the maximum number of dimensions a unit can have
        /// </summary>
        private static readonly int numOfDimensions = Enum.GetValues(typeof (MeasurementTypes)).Length;
        
        /// <summary>
        /// Gets the values for each dimension
        /// </summary>
        private UnitDimension[] _dimensions;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new unit measure
        /// </summary>
        private UnitMeasure()
        {
            _dimensions = new UnitDimension[numOfDimensions];
        }

        /// <summary>
        /// Creates a new unit of measure with the specified dimensions
        /// </summary>
        /// <param name="dimensions"></param>
        public UnitMeasure(params UnitDimension[] dimensions)
            :this()
        {
            if (dimensions.Length == 0)
                throw new ArgumentOutOfRangeException("dimensions");

            foreach (var d in dimensions)
            {
                if (d == null)
                    throw new ArgumentNullException("dimensions");

                this[d.Type] = d;
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the dimension for the specified mesurement type
        /// </summary>
        /// <param name="measure"></param>
        /// <returns></returns>
        public UnitDimension this[MeasurementTypes measure]
        {
            get { return _dimensions[(int) measure]; }
            private set { _dimensions[(int) measure]=value; }
        }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            var top = new StringBuilder();
            var bottom = new StringBuilder();

            bool hasTop=false;
            bool hasBottom = false;

            foreach (var d in _dimensions)
            {
                if (d == null)
                    continue;

                if (d.Order>0)
                {
                    if (hasTop)
                        top.Append("*");

                    top.Append(d.Symbol);
                    if (d.Order>1)
                    {
                        top.Append("^");
                        top.Append(d.Order);
                    }

                    hasTop = true;
                }
                else if (d.Order<0)
                {
                    if (hasBottom)
                        bottom.Append("*");

                    bottom.Append(d.Symbol);
                    if (d.Order<-1)
                    {
                        bottom.Append("^");
                        bottom.Append(Math.Abs(d.Order));
                    }

                    hasBottom = true;
                }
            }

            if (!hasTop && !hasBottom)
                return "count";

            if (!hasBottom)
            {
                return top.ToString();
            }

            if (!hasTop)
            {
                top.Append("1");
            }

            return top + "/" + bottom;
        }

        public bool IsConvertable(UnitMeasure other)
        {
            if (other.Equals(Count))
                return true;

            for (int i = 0; i < numOfDimensions; i++)
            {
                if (_dimensions[i] == null && other._dimensions[i] == null)
                    continue;

                if (_dimensions[i] == null || other._dimensions[i] == null)
                    return false;

                if (_dimensions[i].Order != other._dimensions[i].Order)
                    return false;
            }
            return true;
        }

        public double ToBaseline(double value)
        {
            return _dimensions.Where(d => d != null).Aggregate(value, (current, d) => d.ConversionToBase(current));
        }

        public double FromBaseline(double value)
        {
            return _dimensions.Where(d => d != null).Aggregate(value, (current, d) => d.ConversionFromBase(current));
        }

        //public UnitValue CreateValue()
        //{
        //    return (UnitValue)Runtime.NewUnit(1, this);
        //}

        #endregion

        #region Operators

        /// <summary>
        /// Multiplies unit dimensions to determine the measure for the new result
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static UnitMeasure operator *(UnitMeasure m1,UnitMeasure m2)
        {
            var newMeasure = new UnitMeasure();
            for (int i=0;i<numOfDimensions;i++)
            {
                UnitDimension final = null;
                var d1 = m1._dimensions[i];
                var d2 = m2._dimensions[i];

                if (d1==null)
                    final = d2;
                else if (d2==null)
                    final = d1;
                else 
                    final = d1*d2;

                newMeasure._dimensions[i] = final;
            }
            return newMeasure;
        }

        /// <summary>
        /// Divides a unit dimension to determine the measure for the new result
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static UnitMeasure operator /(UnitMeasure m1, UnitMeasure m2)
        {
            var newMeasure = new UnitMeasure();
            for (int i = 0; i < numOfDimensions; i++)
            {
                UnitDimension final = null;
                var d1 = m1._dimensions[i];
                var d2 = m2._dimensions[i];

                if (d1 == null)
                    final = d2;
                else if (d2 == null)
                    final = d1;
                else
                    final = d1 / d2;

                newMeasure._dimensions[i] = final;
            }
            return newMeasure;
        }

        public static UnitMeasure PowerOperator(UnitMeasure m1, int magnitude)
        {
            var intermediate = m1;
            for(int i=0;i<magnitude;i++)
            {
                intermediate *= m1;
            }
            return intermediate;
        }

        #endregion

        #region Common Units of Measure

        public static readonly UnitMeasure Count = new UnitMeasure();

        #region SI Units

        /// <summary>
        /// Represents a unit of measure in standard meters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly UnitMeasure Meters = new UnitMeasure(UnitDimension.Meter);

        /// <summary>
        /// Represents a measure in square meters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly UnitMeasure SquareMeters = Meters*Meters;


        /// <summary>
        /// Represents a measure in square meters
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly UnitMeasure CubicMeters = Meters*Meters*Meters;

        #endregion

        #endregion
    }
}
