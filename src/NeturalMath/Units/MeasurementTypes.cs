﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath
{
    /// <summary>
    /// Represents the different forms of measurement that can be tracked by the system
    /// </summary>
    public enum MeasurementTypes
    {
        /// <summary>
        /// Describes a unit of measure which describes a linier distance
        /// </summary>
        Length,

        /// <summary>
        /// Describes a unit of measure representing the amount of matter present in a given space
        /// </summary>
        Mass,

        /// <summary>
        /// Describes a unit of measure representing a period in which events occur
        /// </summary>
        Time,

        /// <summary>
        /// Describes a unit of measure representing the amount of thermodynamic energy within matter
        /// </summary>
        Temperature,
    }
}
