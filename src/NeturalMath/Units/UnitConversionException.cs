﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace NeturalMath.Units
{
    /// <summary>
    /// UnitConversionException is thrown whenever a unit attempts to inappropriatly convert or cast
    /// to a diffent unit type which is not equateable
    /// </summary>
    [Serializable]
    public class UnitConversionException : MathException
    {
      
        /// <summary>
        /// Creates a new UnitConversionException
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        public UnitConversionException(ErrorCodes errorCode,string message) 
            : base(errorCode,message)
        {
        }

        /// <summary>
        /// Used internally by .NET to support serialization of exceptions
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected UnitConversionException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
