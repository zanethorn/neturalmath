﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath
{
    /// <summary>
    /// The SystemFunctionAttribute class marks a static method for use as a NeturalMath internal system function
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    internal sealed class SystemFunctionAttribute : Attribute
    {
        /// <summary>
        /// Creates a new instance of the SystemFunctionAttribute class
        /// </summary>
        /// <param name="name">The name of the system function</param>
        public SystemFunctionAttribute(string name)
        {
            Name = name;
        }

        /// <summary>
        /// The name of the system function as used in NeturalMath
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the string description of the function
        /// </summary>
        public string Description { get; set; }

    }
}
