﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System.Linq.Expressions;

namespace NeturalMath.Language
{
    /// <summary>
    /// Represents the parsed form of a piece of code
    /// </summary>
    public class Token
    {
        #region Constructors

        public Token(TokenType type):this(type,null){}

        /// <summary>
        /// Creates a new instance of the token class
        /// </summary>
        /// <param name="type"></param>
        /// <param name="text"></param>
        public Token(TokenType type,string text)
        {
            Type = type;
            if (text != null)
            {
                RawText = text;
                Text = text.Trim();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the token type for this token
        /// </summary>
        public TokenType Type { get; private set;}

        /// <summary>
        /// Gets the trimmed, processed text for this token
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Gets the raw, unprocessed text for this token
        /// </summary>
        public string RawText { get; private set; }

        /// <summary>
        /// The expression resulting from the processed token
        /// </summary>
        public Expression Expression { get; private set; }

        /// <summary>
        /// Determines if the token as an expression associated with it
        /// </summary>
        public bool HasExpression
        {
            get { return Expression != null; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a simple string representation of the token
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}: {1}", Type, Text);
        }
        
        /// <summary>
        /// Sets the expression after the token has been successfully processed
        /// </summary>
        /// <param name="expression"></param>
        internal void SetExpression(Expression expression)
        {
            Expression = expression;
        }

        #endregion
    }
}
