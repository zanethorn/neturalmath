﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Language
{
    public class ListTokenProvider:ITokenProvider
    {
        #region Local Members

        private readonly List<Token> _tokenProvider;
        private int _listIndex = 0;

        #endregion

        #region Constructors

        public ListTokenProvider(List<Token> tokenList)
        {
            _tokenProvider = tokenList;
        }

        #endregion

        #region Public Methods

        public Token GetToken()
        {
            if (_listIndex > _tokenProvider.Count-1)
                return new Token(TokenType.EOF);

            var token= _tokenProvider[_listIndex];
            _listIndex++;
            return token;

        }

        #endregion

    }
}
