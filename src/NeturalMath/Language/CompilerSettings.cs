﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;

namespace NeturalMath.Language
{
    /// <summary>
    /// Represents flags that can be used to modify the compiler/ interpreter behaviors which can be turned on and off
    /// </summary>
    [Flags]
    public enum CompilerSettings
    {
        /// <summary>
        /// No compiler options are set
        /// </summary>
        None                = 0x0000,

        /// <summary>
        /// Compiler is set to silent.  Print and def statements will not be executed
        /// </summary>
        Silent              = 0x0001,

        /// <summary>
        /// Compiler is set to allow allow redundant import statements.  If a file or object which has already been imported is imported again,
        /// the compiler will ignore the second import
        /// </summary>
        AllowImport         = 0x0002,
    }
}
