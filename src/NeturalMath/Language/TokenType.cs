﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

namespace NeturalMath.Language
{
    /// <summary>
    /// Enumerations of token types recognized by the NeturalMath parser
    /// </summary>
    public enum TokenType
    {
        ///// <summary>
        ///// Token is a non-evaluated comment
        ///// </summary>
        //Comment,

        #region Access Modifiers

        /// <summary>
        /// Token to represent a public access modifier
        /// </summary>
        PublicAccess,

        /// <summary>
        /// Token to represent a private access modifier
        /// </summary>
        PrivateAccess,

        /// <summary>
        /// Token to represent a global access modifier
        /// </summary>
        GlobalAccess,

        /// <summary>
        /// Token to represent a constant access modifier
        /// </summary>
        ConstantAccess,

        /// <summary>
        /// Token to represent a read only access modifier
        /// </summary>
        ReadOnlyAccess,

        #endregion

        #region Keywords

        /// <summary>
        /// Token to represent the print keyword
        /// </summary>
        PrintKeyword,

        /// <summary>
        /// Token for the def keyword
        /// </summary>
        DefKeyword,

        /// <summary>
        /// Token for the import keyword
        /// </summary>
        ImportKeyword,

        #endregion

        #region Binary Operators

        /// <summary>
        /// Token to represent a binary add operator
        /// </summary>
        AddOperator,

        SubtractOperator,

        MultiplyOperator,

        DivideOperator,

        PowerOperator,

        ModulusOperator,

        IsEqualOperator,

        NotEqualOperator,

        GreaterThanOperator,

        GreaterThanOrEqualOperator,

        LessThanOperator,

        LessThanOrEqualOperator,

        AndOperator,

        OrOperator,

        XorOperator,

        AssignmentOperator,

        RangeOperator,

        UnitOperator,

        #endregion

        #region Constants

        /// <summary>
        /// Token to represent a numeric constant
        /// </summary>
        NumericConstant,

        /// <summary>
        /// Token to represent a string constant
        /// </summary>
        StringConstant,

        /// <summary>
        /// Token to represent a true constant
        /// </summary>
        TrueConstant,

        /// <summary>
        /// Token to represent a false constant
        /// </summary>
        FalseConstant,

        /// <summary>
        /// Token to represent a void constant
        /// </summary>
        VoidConstant,

        #endregion

        #region Variables & Functions

        /// <summary>
        /// Token represents a call to a variable
        /// </summary>
        Identifier,

        //DotSeperator,

        CommaSeperator,

        SymbolicLookup,

        #endregion

        #region Unary Operators

        DerivitiveOperator,

        IntegralOperator,

        NotOperator,

        FactorialOperator,

        #endregion

        #region Groupings & Domains

        /// <summary>
        /// Token which represents the beginning of a grouping
        /// </summary>
        GroupingBegin,

        /// <summary>
        /// Token which represents the end of a grouping
        /// </summary>
        GroupingEnd,

        /// <summary>
        /// Token which reprsents the beginning of an anyonomyouns domain
        /// </summary>
        DomainBegin,

        /// <summary>
        /// Token which represents the end of a domain
        /// </summary>
        DomainEnd,

        SetBegin,

        SetEnd,

        #endregion

        #region Layout Tokens

        ///// <summary>
        ///// Token which represents an unrecognized or invalid sequence
        ///// </summary>
        //Invalid,
        
        /// <summary>
        /// Token which represents whitespace
        /// </summary>
        //Whitespace,

        NewLine,

        EOF,

        #endregion
    }

}
