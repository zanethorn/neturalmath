﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Language
{
    public interface ITokenProvider
    {
        Token GetToken();
    }
}
