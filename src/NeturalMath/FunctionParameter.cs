﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using NeturalMath.Expressions;

namespace NeturalMath
{
    /// <summary>
    /// Represents a function parameter used by NeturalMath
    /// </summary>
    public sealed class FunctionParameter:MathSymbol
    {
        #region Constructors


        /// <summary>
        /// Creates a new instance of the FunctionParameter class with the provided name and default value
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        public FunctionParameter(string name, Function parentFunction,MathRuntime runtime,ValueExpression defaultValue)
            : base(name, parentFunction, runtime, defaultValue ?? new ConstantValueExpression(runtime.Void, runtime), default(Scope))
        {
        }

        #endregion

    }
}
