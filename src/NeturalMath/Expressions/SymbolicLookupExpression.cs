﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class SymbolicLookupExpression : LookupExpression
    {
        public SymbolicLookupExpression(string memberName, MathRuntime runtime)
            : base(memberName, runtime)
        {
        }

        public override MathValue Execute()
        {
            GetSymbol();
            return new SymbolValue(Symbol,Runtime);
        }

        protected override MathSymbol InnerGetSymbol()
        {
            return Runtime.CurrentDomain.GetSymbol(MemberName);
        }
    }
}
