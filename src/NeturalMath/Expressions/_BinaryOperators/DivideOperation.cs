﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace NeturalMath.Expressions
{
    public sealed class DivideOperation:BinaryOperatorExpression
    {
        #region Constructors

        public DivideOperation(ValueExpression left, ValueExpression right, MathRuntime runtime) 
            : base(left, right, BinaryOperatorType.Divide, runtime)
        {
        }

        #endregion

        #region Helper Methods

        protected override MathValue PerformOperation(MathValue left, MathValue right)
        {
            return left / right;
        }

        #endregion
    }
}
