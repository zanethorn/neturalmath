﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace NeturalMath.Expressions
{
    public sealed class PowerOperation:BinaryOperatorExpression
    {
        #region Constructors

        public PowerOperation(ValueExpression left, ValueExpression right, MathRuntime runtime) 
            : base(left, right, BinaryOperatorType.Power, runtime)
        {
        }

        #endregion

        #region Helper Methods

        protected override MathValue PerformOperation(MathValue left, MathValue right)
        {
            return MathValue.PowerOperator(left, right);
        }

        #endregion
    }
}
