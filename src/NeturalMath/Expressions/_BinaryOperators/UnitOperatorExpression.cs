﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeturalMath.Units;

namespace NeturalMath.Expressions
{
    public class UnitOperatorExpression:BinaryOperatorExpression
    {
        public UnitOperatorExpression(ValueExpression expression, UnitMeasure unit,MathRuntime runtime) 
            : base(expression,new ConstantValueExpression(runtime.NewUnit(1,unit),runtime),BinaryOperatorType.Unit, runtime)
        {
            Unit = unit;
        }

        public UnitMeasure Unit { get; private set; }


        protected override MathValue PerformOperation(MathValue left, MathValue right)
        {
            return left * right;
        }
    }
}
