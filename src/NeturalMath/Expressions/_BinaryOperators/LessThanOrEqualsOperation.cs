﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace NeturalMath.Expressions
{
    public sealed class LessThanOrEqualsOperation:BinaryOperatorExpression
    {
        #region Constructors

        public LessThanOrEqualsOperation(ValueExpression left, ValueExpression right, MathRuntime runtime) 
            : base(left, right, BinaryOperatorType.LessThanOrEqual, runtime)
        {
        }

        #endregion

        #region Helper Methods

        protected override MathValue PerformOperation(MathValue left, MathValue right)
        {
            return left <= right;
        }

        #endregion
    }
}
