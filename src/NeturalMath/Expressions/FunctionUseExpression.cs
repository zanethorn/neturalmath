﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class FunctionUseExpression:LookupExpression
    {
        public FunctionUseExpression(string memberName,IEnumerable<MathExpression> parameters,MathRuntime runtime) 
            : base(memberName,runtime)
        {
            Parameters = parameters;
        }

        public IEnumerable<MathExpression> Parameters { get; private set; }

        public new Function Symbol { get { return base.Symbol as Function; } }

        public override MathValue Execute()
        {
            GetSymbol();
            return Symbol.Invoke(Parameters.Select(p => p.Execute()));
        }

        protected override MathSymbol InnerGetSymbol()
        {
            return Runtime.CurrentDomain.GetFunction(MemberName);
        }
    }
}
