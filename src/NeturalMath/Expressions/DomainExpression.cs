﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class DomainExpression:ValueExpression
    {
        #region Constructors

        public DomainExpression(IEnumerable<MathExpression> expressions, MathRuntime runtime) 
            : base( runtime)
        {
            Expressions = expressions;
        }

        #endregion

        #region Public Properties

        public IEnumerable<MathExpression> Expressions { get; private set; }

        #endregion

        #region Method Overrides

        public override MathValue Execute()
        {
            var result = Runtime.Void;
            foreach (var e in Expressions)
            {
                var r = e.Execute();
                if (r != Runtime.Void)
                    result = r;
            }

            return result;
        }

        #endregion
    }
}
