﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeturalMath.Expressions;

namespace NeturalMath.Expressions
{
    public abstract class VoidExpression:MathExpression
    {

        #region Constructors

        protected VoidExpression(MathRuntime runtime) 
            : base(runtime)
        {
        }

        #endregion

        #region Method Overrides

        public sealed override MathValue Execute()
        {
            PerformActions();
            return Runtime.Void;
        }

        #endregion

        #region Abstract Methods

        protected abstract void PerformActions();

        #endregion
    }
}
