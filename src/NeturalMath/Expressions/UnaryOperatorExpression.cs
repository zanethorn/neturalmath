﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public abstract class UnaryOperatorExpression:ValueExpression
    {
        protected UnaryOperatorExpression(ValueExpression expression,UnaryOperatorType operatorType,MathRuntime runtime) 
            : base(runtime)
        {
            if (expression == null)
                throw new ArgumentNullException("expression");

            Expression = expression;
            OperatorType = operatorType;
        }

        public UnaryOperatorType OperatorType { get; private set; }
        public ValueExpression Expression { get; private set; }
    }
}
