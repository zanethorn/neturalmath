﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class LookupExpression:ValueExpression
    {
        #region Constructors

        public LookupExpression(string memberName,MathRuntime runtime)
            :base(runtime)
        {
            MemberName = memberName;
        }

        #endregion

        #region Public Members

        public string MemberName { get; private set; }

        public MathSymbol Symbol { get; private set; }

        #endregion

        #region Helper Methods

        public override MathValue Execute()
        {
            GetSymbol();
            return Symbol.Invoke();
        }

        public  MathSymbol GetSymbol()
        {
            if (Symbol == null)
                Symbol = InnerGetSymbol();

            return Symbol;
        }

        #endregion

        protected virtual MathSymbol InnerGetSymbol()
        {
            return Runtime.CurrentDomain.GetOrDefineVariable(MemberName);
        }
    }
}
