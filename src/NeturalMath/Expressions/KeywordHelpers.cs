﻿using System;
using System.Linq.Expressions;

namespace NeturalMath.Expressions
{
    /// <summary>
    /// This class is used internally by NeturalMath to help process certain keywords
    /// </summary>
    internal static class KeywordHelpers
    {
        /// <summary>
        /// the Print method used to evaluate an expression and return its value
        /// </summary>
        /// <param name="expression"></param>
        public static void Print(Expression expression)
        {
            var lambda = Expression.Lambda<Func<MathValue>>(expression);
            var del = lambda.Compile();
            var result = del.Invoke();

            Console.WriteLine(result);
        }

        /// <summary>
        /// The method used to import files into the current domain
        /// </summary>
        /// <param name="path"></param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Microsoft.Usage", 
            "CA1801:ReviewUnusedParameters", 
            MessageId = "path",
            Justification="This is temporary"
            )]
        public static void Import(string path)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The method used to print the functional definition of an expression
        /// </summary>
        /// <param name="expression"></param>
        public static void PrintF(Expression expression)
        {
            var visitor = new PrintFVisitor();
            var result = visitor.PrintF(expression);
            
            Console.WriteLine(result);
        }
    }
}
