﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public enum UnaryOperatorType
    {
        UnaryMinus,
        Not,
        Factorial,
    }
}
