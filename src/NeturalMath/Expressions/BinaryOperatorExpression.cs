﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace NeturalMath.Expressions
{
    public abstract class BinaryOperatorExpression:ValueExpression
    {
        #region Constructors

        internal BinaryOperatorExpression(ValueExpression left, ValueExpression right, BinaryOperatorType operatorType, MathRuntime runtime)
            :base(runtime)
        {
            Left = left;
            Right = right;
            OperatorType = operatorType;
        }

        #endregion

        #region Public Properties

        public ValueExpression Left { get; private set; }

        public ValueExpression Right { get; private set; }

        public BinaryOperatorType OperatorType { get; private set; }

        #endregion

        #region Method Overrides

        public override MathValue Execute()
        {
            return PerformOperation(Left.Execute(), Right.Execute());
        }

        protected abstract MathValue PerformOperation(MathValue left, MathValue right);

        #endregion
    }
}
