﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public sealed class FunctionParameterExpression:VoidExpression
    {
        #region Constrctors

        public FunctionParameterExpression(string name, ValueExpression defaultValue,MathRuntime runtime) 
            : base(runtime)
        {
            Name = name;
            DefaultValue = defaultValue;
        }

        public FunctionParameterExpression(string name,  MathRuntime runtime)
            : this(name,null,runtime)
        {
        }

        #endregion

        #region Public Properties

        public string Name { get; private set; }

        public ValueExpression DefaultValue { get; private set; }

        #endregion

        #region Method overrides

        protected override void PerformActions()
        {
            throw new MathException(ErrorCodes.InvalidOperation, "Invalid Operation");
        }

        #endregion
    }
}
