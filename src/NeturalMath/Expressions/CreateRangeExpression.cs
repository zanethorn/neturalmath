﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace NeturalMath.Expressions
{
    public class CreateRangeExpression:BinaryOperatorExpression
    {
        #region Constructors

        public CreateRangeExpression(ValueExpression left,ValueExpression right,MathRuntime runtime)
            :base(left,right,BinaryOperatorType.Range,runtime)
        {
            
        }

        #endregion


        #region Method Overrides

        protected override MathValue PerformOperation(MathValue left, MathValue right)
        {
            var lower = (NumberValue)left;
            var upper = (NumberValue)right;

            return Runtime.NewRange(lower, upper);
        }

        #endregion
    }
}
