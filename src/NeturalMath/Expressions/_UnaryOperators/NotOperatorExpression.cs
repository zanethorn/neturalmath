﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class NotOperatorExpression:UnaryOperatorExpression
    {
        public NotOperatorExpression(ValueExpression expression, MathRuntime runtime) 
            : base(expression,UnaryOperatorType.Not, runtime)
        {
        }

        public override MathValue Execute()
        {
            return !Expression.Execute();
        }
    }
}
