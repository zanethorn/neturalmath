﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class UnaryMinusExpression:UnaryOperatorExpression
    {
        public UnaryMinusExpression(ValueExpression expression,MathRuntime runtime) 
            : base(expression,UnaryOperatorType.UnaryMinus,runtime)
        {
        }

        public override MathValue Execute()
        {
            return -Expression.Execute();
        }
    }
}
