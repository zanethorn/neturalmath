﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class AccessModifierExpression:LookupExpression
    {
        public AccessModifierExpression(LookupExpression lookup, Scope scope,MathRuntime runtime) 
            : base(lookup.MemberName, runtime)
        {
            Scope = scope;
            Lookup = lookup;
        }

        public LookupExpression Lookup { get; private set;}
        public Scope Scope { get; private set; }
    }
}
