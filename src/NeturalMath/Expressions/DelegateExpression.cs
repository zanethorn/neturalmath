﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath.Expressions
{
    public class DelegateExpression:ValueExpression
    {
        #region Constructors

        public DelegateExpression(Func<MathValue> value,MathRuntime runtime) 
            : base(runtime)
        {
            Value = value;
        }

        #endregion

        #region Public Properties

        public Func<MathValue> Value { get; private set; }

        #endregion

        #region Method Overrides

        public override MathValue Execute()
        {
            return Value();
        }

        #endregion
    }
}
