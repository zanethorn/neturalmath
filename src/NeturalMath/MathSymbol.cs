﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using NeturalMath.Expressions;

namespace NeturalMath
{
    public abstract class MathSymbol:DynamicObject
    {
        #region Local Members

        //private Func<MathValue> _innerDelegate;

        #endregion

        #region Constructors

        internal MathSymbol(string name, BlockSymbol parentScope, MathRuntime runtime, ValueExpression value, Scope scope)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            
            Name = name;
            ParentScope = parentScope;
            Value = value;
            Scope = scope;
            Runtime = runtime;
        }

        #endregion


        #region Public Properties

        public string Name { get; private set; }

        public BlockSymbol ParentScope { get; private set; }

        public ValueExpression Value { get; protected set; }

        public Scope Scope { get; private set; }

        public MathRuntime Runtime { get; private set; }

        #endregion

        #region Public Methods

        public virtual MathValue Invoke()
        {
            return Value.Execute();
        }

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
    }
}
