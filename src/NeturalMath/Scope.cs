﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath
{
    public struct Scope
    {
        #region Constructors

        private Scope(AccessabilityLevels accessability, AccessabilityModifiers modifier)
            :this()
        {
            Accessability = accessability;
            Modifier = modifier;
        }

        #endregion

        #region Public Properties

        public AccessabilityLevels Accessability { get; private set; }
        public AccessabilityModifiers Modifier { get; private set; }

        #endregion

        #region Method Overrides

        public override int GetHashCode()
        {
            return (int)Accessability + (int)Modifier;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is Scope))
                return false;

            var other = (Scope) obj;
            return other.Accessability == Accessability && other.Modifier == Modifier;
        }

        #endregion

        #region Static Members

        public static readonly Scope Private = new Scope();
        public static readonly Scope PrivateConst = new Scope(AccessabilityLevels.Private,AccessabilityModifiers.Constant);
        public static readonly Scope Public = new Scope(AccessabilityLevels.Public, AccessabilityModifiers.None);
        public static readonly Scope PublicConst = new Scope(AccessabilityLevels.Public, AccessabilityModifiers.Constant);
        public static readonly Scope PublicReadOnly = new Scope(AccessabilityLevels.Public, AccessabilityModifiers.ReadOnly);
        public static readonly Scope Global = new Scope(AccessabilityLevels.Global, AccessabilityModifiers.None);
        public static readonly Scope GlobalConst = new Scope(AccessabilityLevels.Global, AccessabilityModifiers.Constant);

        #endregion
    }
}
