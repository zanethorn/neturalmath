﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Runtime.Serialization;

namespace NeturalMath
{
    /// <summary>
    /// The base class for all NeturalMath internal exceptions
    /// </summary>
    [Serializable]
    public class MathException : Exception
    {
        /// <summary>
        /// Creates a new math exception
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        public MathException(ErrorCodes errorCode,string message) 
            : base(message)
        {
            ErrorCode = errorCode;
        }

        /// <summary>
        /// Creates a new general exception based on the internal exception
        /// </summary>
        /// <param name="inner"></param>
        public MathException(Exception inner) : base(inner.Message, inner)
        {
            ErrorCode = ErrorCodes.GenericError;
        }

        /// <summary>
        /// Used internally by .NET for seralizing exceptions
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected MathException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }

        /// <summary>
        /// Returns the error code associated with this exception
        /// </summary>
        public ErrorCodes ErrorCode { get; private set; }

        /// <summary>
        /// Used internally by .NET to support serialization for exceptions
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
