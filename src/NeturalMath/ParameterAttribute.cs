﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeturalMath
{
    /// <summary>
    /// The ParameterAttribute class indicates the NeturalMath parameters which are provided to a system function
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public sealed class ParameterAttribute : Attribute
    {
        /// <summary>
        /// Creates a new instance of the ParameterAttribute class
        /// </summary>
        /// <param name="name">The name used by NeturalMath for the parameter</param>
        public ParameterAttribute(string name)
        {
            Name = name;
            DefaultValue = "void";
        }

        /// <summary>
        /// The name of the parameter as used in NeturalMath
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The NeturalMath expression representing the default value of the parameter.
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// The value type(s) allowed for this parameter
        /// </summary>
        public ValueTypes ParameterType { get; set; }

        /// <summary>
        /// Determines the order of the parameter
        /// </summary>
        public int Ordinal { get; set; }
    }
}
