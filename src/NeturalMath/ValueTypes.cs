﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;

namespace NeturalMath
{
    /// <summary>
    /// Represents the data type of a MathValue variable
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")]
    [ System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2217:DoNotMarkEnumsWithFlags")]
    [Flags]
    public enum ValueTypes:int
    {
        /// <summary>
        /// Data type is unknown.  This is commonly for user defined types, or .NET interop types
        /// </summary>
        Unknown = 1<<-1,

        /// <summary>
        /// Data is a VoidValue.  Void has no instance information
        /// </summary>
        Void        = 0,

        /// <summary>
        /// Data is a NumberValue.  Represents numeric data.
        /// </summary>
        Number      = 1,

        /// <summary>
        /// Data is a StringValue.  Represents text data
        /// </summary>
        String      = 1<<1,

        /// <summary>
        /// Data is a BooleanValue.  Represents true/false data
        /// </summary>
        Bool        = 1<<2,

        /// <summary>
        /// Data is a ComplexValue.  Data has both a real and imaginary part
        /// </summary>
        Complex     = 1<<3,

        /// <summary>
        /// Data is a RangeValue.  Data has an upper and lower bound.
        /// </summary>
        Range = 1<<4,

        /// <summary>
        /// Data is a UnitValue.  Data is numeric, but with a quantified value
        /// </summary>
        Unit = 1<<5,

        /// <summary>
        /// Data is a SetValue.  Data is a list of arbirary values.
        /// </summary>
        Set = 1<<6,

        /// <summary>
        /// Data is an ExpressionValue.  Represents a late-bound expression value
        /// </summary>
        Symbol = 1<<7,


        #region Composite Values

        /// <summary>
        /// Represents the composite of all Numbers, Unites, Complex and Range
        /// </summary>
        AllNumeric = 57,

        /// <summary>
        /// Represents all built in types except for voids
        /// </summary>
        BuiltIn = 2047,

        #endregion
    }
}