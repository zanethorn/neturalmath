﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeturalMath.Properties;

namespace NeturalMath
{
    public sealed class SymbolValue:MathValue
    {
        internal SymbolValue(MathSymbol value,MathRuntime runtime) 
            : base(ValueTypes.Symbol, "symbol", value,runtime)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            Value = value;
        }

        public new MathSymbol Value { get; private set; }

        protected override MathValue Add(MathValue arg)
        {
            var value = Value.Invoke();
            return value + arg;
        }

        protected override MathValue Subtract(MathValue arg)
        {
            var value = Value.Invoke();
            return value - arg;
        }

        protected override MathValue Multiply(MathValue arg)
        {
            var value = Value.Invoke();
            return value * arg;
        }

        protected override MathValue Divide(MathValue arg)
        {
            var value = Value.Invoke();
            return value / arg;
        }

        protected override MathValue Modulo(MathValue arg)
        {
            var value = Value.Invoke();
            return value % arg;
        }

        protected override MathValue Power(MathValue arg)
        {
            throw new NotImplementedException();
        }

        protected override MathValue And(MathValue arg)
        {
            var value = Value.Invoke();
            return value && arg;
        }

        protected override MathValue Or(MathValue arg)
        {
            var value = Value.Invoke();
            return value || arg;
        }

        protected override MathValue Xor(MathValue arg)
        {
            var value = Value.Invoke();
            return value ^ arg;
        }

        protected override bool IsTrue()
        {
            throw new NotImplementedException();
        }

        protected override MathValue ToMinus()
        {
            throw new NotImplementedException();
        }

        protected override MathValue Flip()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToString()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToNumber()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToBoolean()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToRange()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToComplex()
        {
            throw new NotImplementedException();
        }

        protected internal override MathValue ConvertToUnit(Units.UnitMeasure measure)
        {
            throw new NotImplementedException();
        }

        protected override MathValue ConvertToNativeValue(MathValue arg)
        {
            throw new NotImplementedException();
        }

        protected override string GetToString()
        {
            throw new NotImplementedException();
        }

        protected override bool? IsLessThan(MathValue arg)
        {
            throw new NotImplementedException();
        }

        protected override bool? IsGreaterThan(MathValue arg)
        {
            throw new NotImplementedException();
        }
    }
}
