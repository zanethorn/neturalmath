﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Runtime.Serialization;

namespace NeturalMath
{
    /// <summary>
    /// Used by NeturalMath when a value cannot be converted to another value type
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors"), Serializable]
    public class CastException:MathException
    {
        /// <summary>
        /// Creates a new instance of the cast exception
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="message"></param>
        public CastException(ErrorCodes errorCode, string message) 
            : base(errorCode, message)
        {
        }

        /// <summary>
        /// used internally by .NET to serialize cast exception
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected CastException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
