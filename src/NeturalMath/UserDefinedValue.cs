﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;

namespace NeturalMath
{
    /// <summary>
    /// The base class for extending NeturalMath with new value types.  This class should be used
    /// to add custom data types to the NeturalMath engine.
    /// </summary>
    public abstract class UserDefinedValue:MathValue
    {
        #region Constructor

        /// <summary>
        /// Creates a user defined value
        /// </summary>
        /// <param name="typeName">The simple string name for this data type</param>
        /// <param name="value">The object data represented by this data type</param>
        protected UserDefinedValue(string typeName, object value,MathRuntime runtime)
            :base(ValueTypes.Unknown,typeName,value,runtime)
        {
            if (value == null)
                throw new ArgumentNullException("value");
            if (string.IsNullOrWhiteSpace(typeName))
                throw new ArgumentNullException("typeName");
        }

        #endregion
    }
}
