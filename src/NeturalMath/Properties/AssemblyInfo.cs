﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NeturalMath")]
[assembly: AssemblyDescription("A .NET library for parsing, evaluating and executing natural mathematical statements.")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("92432c44-5b56-4c38-855a-1967e6b8fd53")]



[assembly:InternalsVisibleTo("NeturalMath.UnitTests,PublicKey=0024000004800000940000000602000000240000525341310004000001000100d715a3e48552da792a170d477168c53d3080e1619344d8209ef8ff49ddc01253048b1a81506b42befac3e32c118f680db2dba18fb4e0927630a20c7599f87610529cd373b354d7d10692ee747560f37e9caa30b933c1ef98dca1f2286a54fc762cc6729401e3e1253598ff7c11b6a0f881ac3af3730234f1ab9b97b165a316c8")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
