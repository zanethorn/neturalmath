﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using MathConsole.Properties;
using Microsoft.Win32;

namespace MathConsole
{
    public abstract class ConsoleInterface
    {
        private const string OPEN_SAVE_FILTER = "MATH files|.math|All Files|*.*";

        protected List<string> Buffer = Program.TextBuffer;

        public abstract bool Read();

        protected bool DoExit()
        {
            return false;
        }


        public void Save(string path = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                var dialog = new SaveFileDialog();
                dialog.Filter = OPEN_SAVE_FILTER;

                if (dialog.ShowDialog()==false)
                {
                    Console.WriteLine(Resources.SaveCanceled);
                    return;
                }
                path = dialog.FileName;
            }

            File.WriteAllLines(path, Buffer);

            
        }

        public void Open(string path = null)
        {
            
        }

        protected void DisplayHelp(string input)
        {
            Console.WriteLine(Resources.NoHelp);
        }
    }
}
