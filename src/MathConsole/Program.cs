﻿/********************************************************************************
* Copyright 2010 Zane Thorn (zane.thorn@gmail.com)                              *
*                                                                               *
* NeturalMath is free software: you can redistribute it and/or modify           *
* it under the terms of the GNU Lesser General Public License as published by   *
* the Free Software Foundation, either version 3 of the License, or             *
* (at your option) any later version.                                           *
*                                                                               *
* NeturalMath is distributed in the hope that it will be useful,                *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 *
* GNU Lesser General Public License for more details.                           *
*                                                                               *
* You should have received a copy of the GNU Lesser General Public License      *
* along with NeturalMath.  If not, see <http://www.gnu.org/licenses/>.          *
********************************************************************************/


//#define TRACKING
#define VERBOSE
//#define SIMPLIFY

using System;
using System.Collections.Generic;
using System.Reflection;
using MathConsole.Properties;
using NeturalMath;
using NeturalMath.Language;

namespace MathConsole
{
    static class Program
    {
        public static ConsoleModes Mode { get; private set; }
        public static List<string> TextBuffer { get; private set; }
        public static ConsoleInterpreter Interpreter { get; private set; }
        public static ConsoleEditor Editor { get; private set; }
        public static ConsoleInterface CurrentInterface { get; private set; }

        public static MathRuntime Runtime { get; private set; }

        static void Main(string[] args)
        {
            TextBuffer = new List<string>();

            Runtime = new MathRuntime();

            Interpreter = new ConsoleInterpreter();
            Editor = new ConsoleEditor();

            SwitchToInterpreter();

            var version = GetVersion();
            Console.Write(string.Format(Resources.ConsoleHeader, version));
            Console.WriteLine();

            bool result=false;
            do
            {
                result = CurrentInterface.Read();
            } while (result); 
        }

        private static Version GetVersion()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var name = assembly.GetName();
            var version = name.Version;

            Console.Title = string.Format("{0} v{1} - {2}", name.Name, version, Mode);
            return version;
        }


        public static void SwitchToEditor()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            Mode = ConsoleModes.Editor;
            CurrentInterface = Editor;

            if (TextBuffer.Count == 0)
                TextBuffer.Add(string.Empty);

            Editor.Redraw();
            GetVersion();
        }

        public static void SwitchToInterpreter()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            Mode = ConsoleModes.Interpreter;
            CurrentInterface = Interpreter;

            GetVersion();
        }
    }
}
