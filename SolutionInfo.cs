﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("0.2.5.0")]
[assembly: AssemblyFileVersion("0.2.5.0")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Newtonian Physics")]
[assembly: AssemblyProduct("NeturalMath")]
[assembly: AssemblyCopyright("Copyright © Newtonian Physics 2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]